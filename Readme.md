# About 

This fork of `hyperion-projects` is an example haskell program for numerical
conformal bootstrap computations with support for spinning conformal blocks via `blocks_3d`.
It includes example computations for sigma-epsilon mixed correlator system in 3d 
Ising CFT and 3d four-fermion single correlator system. It uses the
[hyperion](https://github.com/davidsd/hyperion/) cluster management
library, the [hyperion-bootstrap](https://gitlab.com/davidsd/hyperion-bootstrap) library,
and the [sdpb-haskell](https://gitlab.com/davidsd/sdpb-haskell/).

Note: this is everyday working code, and it may change without notice.

# Usage

- Before compiling, you must create `src/Config.hs` and prepare the appropriate
  scripts so that `hyperion-projects` can use `sdpb` and other programs on your machine.
  The recommended way to do this is:

      cp -r config/caltech-hpc-dsd config/my-special-machine
      # edit 'config/my-special-machine/Config.hs' as necessary
      # edit the scripts in 'config/my-special-machine' as necessary
      cd src
      ln -s ../config/my-special-machine/Config.hs Config.hs

- For `hyperion` to work properly, your nodes should be able to `ssh` into
  other nodes without a password promt or any other interactive steps. This might
  require you to set up a key pair without a passphrase. Also see the [documentation
  for SSHCommand](https://davidsd.github.io/hyperion/hyperion/Hyperion-WorkerCpuPool.html#t:SSHCommand)
  for details on how `ssh` is called. You might need to change it in your `Config.hs`
  file (described above).
  
- To test, try

      ./hyp IsingSigEpsAllowed_test_nmax6
      
  or
        
      ./hyp FourFermions3dAllowed_test_nmax6
  
  This will run two computations requiring 6 cores each. The computations test
  feasibility of two points in dimension space (Delta_sigma, Delta_epsilon) for
  mixed correlators of sigma and epsilon in the 3d Ising model or (Delta_psi, Delta_eps)
  in single correlator in four fermions. One point is
  allowed (primal feasible) and the other is disallowed (dual feasible).
  
  The output of `./hyp` is printed to a file `nohup.out`, and that includes a path
  to a more detailed log file which you can inspect to monitor the computation.
  
  The computation "IsingSigEpsAllowed_test_nmax6" is defined in the file
  `Projects/IsingSigEpsTest2020.hs`. 
  The computation "FourFermions3dAllowed_test_nmax6" is defined in the file
  `Projects/FourFermions3dTest2020.hs`. These files also define other calculations
  you may want to try.
  
  Results of the calculations are written to log files and a database (specified
  in `nohup.out`). For more details on how this happens, see the code of this
  repository and the libraries, as well as the haddock to `hyperion` (available
  as standalone [here](https://davidsd.github.io/hyperion)).
  
  For the general logic of cluster management, the `hyperion` library
  contains [examples](https://github.com/davidsd/hyperion/tree/master/example) which may be instructive.

# Installation notes

- This repository uses git submodules, so to clone the repository, use `git clone --recursive https://gitlab.com/pkravchuk/hyperion-projects.git`.
	If you have already cloned the repository without the submodules, use `git submodule update --init --recursive`.

- If you want to use Delaunay search, you need the [qhull](http://www.qhull.org/download/) package. It's probably best to follow the instructions
	for installation using `cmake`.

- To compile the executable, run `stack build`. Note that the login nodes for some clusters may not have enough memory to do the compilation. In this case, it's advised to run `stack build -j1`
	on a compute node as necessary. For reference, Yale's Grace cluster took this command: `srun -N 1 -t 04:00:00 --mem=220G stack build -j1`.

- One of the dependencies needs a libblas.o and liblapack.o. If this is causing compile problems and your cluster uses OpenBLAS or something similar, run something like
	`ln -s /path/to/lib/libopenblas.o /another/path/to/lib/libblas.o` and `ln -s /path/to/lib/libopenblas.o /another/path/to/lib/liblapack.o`,
	and add `/another/path/to/lib` to the extra-lib-dirs of `stack.yaml`.


# Notes on git and submodules
-----

See [this file](Submodules.md).

# Other comments


- Configuration is compiled into the program for the following reason:
  Many copies of the executable are run on different machines over the
  course of a computation. If the configuration were read when the
  program started up, one could have different instances of the program
  with different configurations, all communicating with each other.


# Implementation notes and reading

## Finding Haskell definitions

[Hoogle](https://hoogle.haskell.org/), the Haskell search engine, can be a great resource.

## Phantom types, `Tagged` and `Reflection`

The `Bounds` modules make extensive use of phantom types, `Tagged`, `Reifies` to ensure that data like 
external operator dimensions is consistent. A phantom type is a type that is used to label values but does not appear in the actual data. It is a way of leveraging the type system to constrain the way data is used.

Here's an excellent introduction to using phantom types to make mathematics more typesafe: [Implicit Configurations — or, Type Classes Reflect the Values of Types](http://okmij.org/ftp/Haskell/tr-15-04.pdf). The [documentation for Data.Tagged](https://hackage.haskell.org/package/tagged-0.8.6/docs/Data-Tagged.html) and [documentation for Data.Reflection](https://hackage.haskell.org/package/reflection-1.5.1.1) are also useful resources.

## The `Fetches` `Applicative`

The `Fetches` `Applicative` in `sdpb-haskell` uses the fact that an applicative computation that is polymorphic in the type of `Applicative`, i.e. `forall f. Applicative f => (b -> f a) -> f a` can be inspected to determine its dependencies (of type `b`). This idea is nicely explained in [Build Systems a la Carte](https://www.microsoft.com/en-us/research/uploads/prod/2018/03/build-systems.pdf).
