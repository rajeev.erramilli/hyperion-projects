# Notes on git and submodules
-----

This repository has several submodules in the `lib` directory. Each
submodule is currently on the `master` branch. This file contains some important details on
working with submodules for this project.

## Basics of submodules

Intrinsically, a git submodule is just a git repository. This means that, for example,
while we have a submodule in `lib/hyperion` linked to the `hyperion` repository, `hyperion`
repository never has to know that it is used as a submodule in `hyperion-projects`. This is
one invariant that is useful to keep in mind.

Extrinsically, i.e. from the point of view of the superproject, `hyperion-projects`,
submodules are something more clever than just subdirectories. In particular, `hyperion-projects`
knows that `lib/hyperion` is its own git repository, and so keeps some extra information about it.

The most important thing is that `hyperion-projects` contains references to the "latest" commits of all its
submodules. That is, it has an opinion about what is the commit that it should use from, say, `hyperion` repository
in `lib/hyperion`. This reference is used, for example, when you (properly, as described in [readme](Readme.md)) clone the `hyperion-projects` repository
to populate `lib/hyperion` directory, or use some update commands.

Importantly, this reference is literally the reference for a commit in `hyperion` that was specifically recorded in `hyperion-projects`. It does not have to match or follow any particular branch. This commit can be far behind any remote or local branches in `hyperion`.
It may also not exist in your local version of `hyperion` (stored in `lib/hyperion`) or even on remote.

Also, the fact that there is a reference to a `hyperion` commit recorded in `hyperion-projects` does not actually
mean much. After all, `hyperion` is still a git repo and you can checkout any commit from its entire history inside
of `lib/hyperion`, which can be in the past or in the future of this reference.

So we could in principle forget about the existence of this reference and just agree to always use the latest versions of
some particular branches in submodules. However, its not good to have implicit assumptions, and it would also make our life harder
because we won't be able to clone `hyperion-projects` easily and use some other features.

So what we want to do is to keep these references in `hyperion-projects` up-to-date and make sure that they point to
the latest submodule commits (e.g. heads of master branches) that `hyperion-projects` is supposed to work with. There are
several ways in which it is easy to make a mistake while doing this, which are described below.

In the end, using git submodules amounts to working with submodules as normal git repositories and maintaining good submodule references
in `hyperion-projects`. If you feel adventurous, you can use some fancier git commands to make your life easier (some described below),
but in general that's all that essential.

## Getting the latest versions

Before making changes to the code, do a pull to make sure you have the
latest version. With submodules this can be a bit tricky. I recommend
the following:

Go to each submodule folder and pull the latest version, for example:

    > cd ../lib/hyperion
    > git pull origin master

Then pull the latest version from the main repository

    > cd ../.. # or wherever the hyperion-projects repo is
    > git pull origin master
    > git status # it's a good idea to type this a lot

You might see `detached HEAD` message. This is fine, but if you want to
make changes to the code in this submodule, you will need to checkout the
appropriate branch, e.g.

    >  git checkout master

This will get you out of `detached HEAD` state. Its easier not to do commits
in `detached HEAD` state unless you know what you're doing.

## Making a change to `hyperion-projects`

To make a change to `hyperion-projects` (as opposed to one of the
submodules in the lib directory): First edit the files as appropriate
using your favorite editor. When you are ready to commit, make sure to
first build the code to make sure it compiles:

    > stack build

Fix any compilation errors before committing. Now to commit your
changes, do

    > git status 

This will show which files have been changed.

Run `git add` on all of the changed files that you want to commit

    > git add src/Bounds/GNY.hs
    > git add src/Bounds/FourFermions3d.hs
    > ...

If you want to add all the changes made inside the `src` directory,
you can just do

    > git add src

You can also do things like `git add --all` -- check the git manual.

If you have edited any of the code in the `lib` directories, follow
the instructions in the "Making a change to `lib`s" section, preferably before
you proceed with commiting `hyperion-projects`.

Finally, after all changed files and libs have been added, run

    > git commit -m "Here's a message explaining my changes"

Now push your changes

    > git push origin master

If you get an error message, it's probably because the upstream
repository has been changed. Follow the instructions in the "Getting
the latest versions" section to pull the latest changes and merge them
with your own. Then run

    > git push origin master

again. Make sure you do the same for any changes you have made to
submodules. (See instructions in the next section.)

## Making a change to a submodule in `lib`

If you have changed some code in a submodule, you should add, commit,
and push your changes in each just as if they were independent
repositories. For example, suppose we have edited code in
`sdpb-haskell`. The steps are: make sure the code compiles

    > stack build 

(It's usually best to run this in the hyperion-projects directory to
make sure the whole thing compiles, but if you want to save time, you
can rin it in the `sdbp-haskell` directory to just build the library).

    > cd lib/sdpb-haskell/
    > git status # see what has been changed

If you see `detached HEAD` state message, then you need to checkout the
appropriate branch before commiting, using

    > git checkout master

if you want to commit to the `master` branch. If this command fails,
this means that something's wrong (your `HEAD` is pointing to a commit
that is sufficiently different from where `master` points, which should
not happen with the simple workflows described in this document unless you
or another collaborator made a mistake). Generally it is better to make
sure you're on the desired branch before editing things.

You can now proceed to commit your changes

    > git add .... whatever files you changed ....
    > git commit -m "Explain your changes"
    > git pull origin master # get the latest version and merge it with your own
    > git push origin master
    > git status # make sure everything looks good

Now you'll have to add your changes to the main repository. The
hyperion-projects repository only keeps track of the commit hash of
the submodules in `lib`. You need to update that commit hash as
follows:

    > cd ../.. # go to the root directory for hyperion-projects
    > git status # if you have changed a submodule, you should see something like "lib/sdpb-haskell (new commits)"
    > git add lib/sdpb-haskell
    > git commit -m "Updated sdpb-haskell"
    > git pull origin master
    > git status # make sure everything looks ok 
    > git push origin master
    > git status # make sure everything looks ok

## Things that can go wrong

The above instructions try to keep the `hyperion-projects`'s references to `lib/` submodule commits up-to date with the `origin/master`.
This requires making a commit to `hyperion-projects` after updating a submodule. 

One thing that often happens is that someone forgets to do this.
What this means is that the reference in `hyperion-projects` is older than the latest version of some submodule. This usually is not a problem if
all collaborators update their submodles to latest branch versions as described above. However, this may become a problem if someone clones `hyperion-projects` onto a new cluster, say, and tries to build it without updating individual submodules by hand.

A worse scenario is when someone forgets to **push** a change in submodule but updates the reference in `hyperion-projects` by commiting and pushing there. In this case the other users will get error messages when they pull, and they won't have the new code from the submodule (without which `hyperion-projects` may not compile). To help prevent this behavior, one may run 

    > git config push.recurseSubmodules check

in `hyperion-projects` to force a check of submodules having been pushed before `hyperion-projects` is allowed to be pushed.

## Alternative commands to use with submodles

(PK: Have been using this for a bit, YMMW.)

An easy way to get all submodules updated is to do

    > git submodule update --remote

For this to work in the same way as doing `git pull` in each submodule, `.gitmodules` has to be configured property, which it hopefully is.
Omitting `--remote` will use the superproject's references instead of the default branches specified in `.gitmodules`. Whatever it uses, it will
try to merge that into the current `HEAD` in the submodule. So perhaps keep the `--remote` and the `HEAD`s should perhaps be attached to the same branch as specified in `.gitmodules` (`master` for all submodles at the time of writing) unless you like lots of merge conflicts.

You can also try using 

    > git submodule foreach <command>

to execute `command` in each submodule. For example,

    > git submodule foreach 'git checkout master'
    > git submodule foreach 'git pull'

will checkout master branch in each submodule (useful if you've just cloned the repository) and then do git pull everywhere.
Similarly, you can do

    > git submodule foreach 'git status'

to get status report from every repository.

As usual, you can use `ssh-agent` to store your credentials, e.g. if you have a passphrase for your ssh key, do

    > eval $(ssh-agent)
    > ssh-add

this will save you from having to enter the passphrase for every submodule when running the above commands. Will forget your
passphrase once `ssh-agent` is killed.
