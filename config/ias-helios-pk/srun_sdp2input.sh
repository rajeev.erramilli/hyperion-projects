#!/bin/bash

module purge
#source $HOME/local/modules/load_openmpi_modules
module load openmpi/gcc/3.0.0/64
module load gcc/7.3.0

LD_LIBRARY_PATH=/home/pkravchuk/local/lib:/home/pkravchuk/repos/elemental/build/install/lib:$LD_LIBRARY_PATH

srun --mpi=pmix_v1 /home/pkravchuk/repos/sdpb/build/sdp2input $@
