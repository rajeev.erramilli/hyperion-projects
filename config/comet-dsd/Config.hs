-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed          (makeRelativeToProject, strToExp)
import           Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))
import           System.FilePath.Posix   ((</>))

-- Used for smaller files, including logs, databases, and executables.
storageDir :: FilePath
storageDir = shortTermStorageDir
--storageDir = "/home/dsd/hyperion-data"

-- Used for big files like sdp files, checkpoints, etc.  The scratch
-- filesystem seems to work best for this kind of thing.
shortTermStorageDir :: FilePath
shortTermStorageDir = "/oasis/scratch/comet/dsd/temp_project/hyperion"

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "config/comet-dsd" >>= strToExp)

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "dsd@caltech.edu"

    -- Working directory for data files
  , dataDir = shortTermStorageDir </> "data"

    -- Save logs to this directory
  , logDir = storageDir </> "logs"

    -- Directory for databases used during computation
  , databaseDir = storageDir </> "databases"

    -- Hyperion makes a (temporary) copy of its executable in order to run
    -- remote copies of itself. Store executables in this directory
  , execDir = storageDir </> "executables"

    -- Directory for SLURM jobs
  , jobDir = storageDir </> "jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = scriptsDir </> "srun_sdpb.sh"

    -- Path to a script that runs sdp2input, after loading
    -- appropriate modules and setting environment variables
  , srunSdp2inputExecutable = scriptsDir </> "srun_sdp2input.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = scriptsDir </> "sdpb.sh"

    -- Path to a script that runs sdp2input (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdp2inputExecutable = scriptsDir </> "sdp2input.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = scriptsDir </> "scalar_blocks.sh"

    -- Path to a script that runs blocks_3d, after loading appropriate
    -- modules and setting environment variables
  , blocks3dExecutable = scriptsDir </> "blocks_3d.sh"

    -- Path to a script that runs blocks_4d, after loading appropriate
    -- modules and setting environment variables
  , blocks4dExecutable = ""

    -- Path to a script that runs seeds_4d, after loading appropriate
    -- modules and setting environment variables
  , seeds4dExecutable = ""

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/home/dsd/src/qhull-2015.2/bin/qdelaunay"

    -- Path to a script that runs tiptop
  , tiptopExecutable = scriptsDir </> "tiptop.sh"

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that.
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to.
  , defaultSlurmPartition = Just "compute"

    -- Default SLURM account.
  , defaultSlurmAccount = Nothing

    -- Default command for running ssh
  , sshRunCommand = Nothing
  }

