-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed          (makeRelativeToProject, strToExp)
import           Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))
import           System.FilePath.Posix   ((</>))

storageDir :: FilePath
storageDir = "/global/cscratch1/sd/davidsd/hyperion"

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "config/nersc-dsd" >>= strToExp)

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "dsd@caltech.edu"

    -- Working directory for data files
  , dataDir = storageDir </> "data"

    -- Save logs to this directory
  , logDir = storageDir </> "logs"

    -- Directory for databases used during computation
  , databaseDir = storageDir </> "databases"

    -- Hyperion makes a (temporary) copy of its executable in order to run
    -- remote copies of itself. Store executables in this directory
  , execDir = storageDir </> "executables"

    -- Directory for SLURM jobs
  , jobDir = storageDir </> "jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = scriptsDir </> "srun_sdpb.sh"

    -- Path to a script that runs sdp2input, after loading
    -- appropriate modules and setting environment variables
  , srunSdp2inputExecutable = scriptsDir </> "srun_sdp2input.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = scriptsDir </> "sdpb.sh"

    -- Path to a script that runs sdp2input (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdp2inputExecutable = scriptsDir </> "sdp2input.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = scriptsDir </> "scalar_blocks.sh"

    -- Path to a script that runs blocks_3d, after loading appropriate
    -- modules and setting environment variables
  , blocks3dExecutable = scriptsDir </> "blocks_3d.sh"

    -- Path to a script that runs blocks_4d, after loading appropriate
    -- modules and setting environment variables
  , blocks4dExecutable = ""

    -- Path to a script that runs seeds_4d, after loading appropriate
    -- modules and setting environment variables
  , seeds4dExecutable = ""

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = ""

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , tiptopExecutable = ""

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that. Default: Nothing
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to. Default: Nothing
  , defaultSlurmPartition = Nothing

    -- Default SLURM account. Default: Nothing
  , defaultSlurmAccount = Nothing

    -- Default command for running ssh
  , sshRunCommand = Nothing
  }
