{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.SingletScalar2020 where

import           Bounds.SingletScalar                        (SingletScalar (..))
import qualified Bounds.SingletScalar                        as SS
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Functor.Identity                       (Identity (..))
import           Data.Proxy                                  (Proxy (..))
import           Data.Scientific                             (Scientific)
import           Data.Text                                   (Text)
import           Data.Vector                                 (Vector)
import qualified Data.Vector                                 as V
import           GHC.Generics                                (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                              Bracket (..))
import           Hyperion.Bootstrap.CFTBound                 (BinarySearchBound (..),
                                                              BoundFileTreatment (..),
                                                              CFTBound (..),
                                                              CFTBoundFiles (..),
                                                              FileTreatment (..),
                                                              defaultCFTBoundFiles,
                                                              getBoundObject,
                                                              keepAllFiles,
                                                              reifyPrecisionWithFetchContext,
                                                              remoteBinarySearchBound,
                                                              remoteComputeBound,
                                                              remoteComputeBoundWithFileTreatment,
                                                              toSDP)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (blockParamsNmax,
                                                              jumpFindingParams,
                                                              optimizationParams,
                                                              spinsNmax)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import qualified Hyperion.Database                           as DB
import qualified Hyperion.Log                                as Log
import           Hyperion.Util                               (hour, minute)
import           Numeric.Rounded                             (Rounded,
                                                              RoundingMode (..))
import           Projects.Defaults                           (defaultBoundConfig)
import           SDPB.Blocks.Delta                           (Delta (..))
import qualified SDPB.Blocks.ScalarBlocks                    as SB
import           SDPB.Bounds.Spectrum                        (setGap,
                                                              unitarySpectrum)
import qualified SDPB.Math.DampedRational                    as DR
import qualified SDPB.Math.VectorSpace                       as VS
import qualified SDPB.SDP.Types                              as SDP
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB

data SingletScalarBinarySearch = SingletScalarBinarySearch
  { ssbs_bound     :: CFTBound Int SingletScalar
  , ssbs_config    :: BinarySearchConfig Rational
  , ssbs_gapSector :: Int
  } deriving (Show, Generic, Binary, ToJSON)

remoteSingletScalarBinarySearch :: SingletScalarBinarySearch -> Cluster (Bracket Rational)
remoteSingletScalarBinarySearch ssbs = remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ssbs_bound ssbs) `cAp` cPure (ssbs_gapSector ssbs)
  , bsConfig            = ssbs_config ssbs
  , bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound cftBound gapSector gap =
      cftBound { bound = (bound cftBound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (bound cftBound))
    getBool cftBound files result = do
      case SDPB.isDualFeasible result of
        False -> pure ()
        True  -> saveVectors cftBound files result [0,2,4,6,8]
      -- For an optimization, return whether the functional acting on
      -- the unit operator is less than zero.
      pure $ SDPB.isDualFeasible result && SDPB.dualObjective result < 0

-- TODO: Generalize this function away from SingletScalar
saveVectors :: CFTBound Int SingletScalar -> CFTBoundFiles -> SDPB.Output -> [Int] -> Job ()
saveVectors cftBound' boundFiles result spins =
  reifyPrecisionWithFetchContext cftBound' (Proxy @Job) (precision (cftBound' :: CFTBound Int SingletScalar))
  $ \(_ :: Proxy p) -> do
  let cftB = (cftBound' :: CFTBound Int SingletScalar) { precision = Proxy @p }
  norm <- getBoundObject @Job @SingletScalar @p @(Vector (Rounded 'TowardZero p)) cftB boundFiles $
    SDP.normalizationVector . SDP.sdpNormalization . toSDP
  alphaVec <- liftIO $ SDPB.readFunctional norm (outDir boundFiles)
  let getVec l = getBoundObject cftB boundFiles $
        SS.getInternalBlockVec (SB.SymTensorRep (RelativeUnitarity 0) l)
      getAlphaAction = fmap (DR.mapNumerator (Identity . dotPols alphaVec)) . getVec
  alphaActions <- traverse getAlphaAction spins
  Log.info "Functional" $ zip spins alphaActions
  mapM_ (\(l,f) -> DB.insert functionalActions (cftBound',l) (f, SDPB.dualObjective result)) $
    zip spins alphaActions
  where
    dotPols v pv = VS.sum $ V.zipWith (VS.*^) v pv
    functionalActions :: DB.KeyValMap (CFTBound Int SingletScalar, Int) (DR.DampedRational base Identity a, Scientific)
    functionalActions = DB.KeyValMap "functional_actions"

singletScalarDefaultGaps :: Int -> Rational -> SingletScalar
singletScalarDefaultGaps nmax dPhi = SingletScalar
  { spectrum     = unitarySpectrum
  , objective    = SS.Feasibility
  , blockParams  = blockParamsNmax nmax
  , spins        = spinsNmax nmax
  , deltaPhi     = dPhi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "SingletScalarAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [ (0.5181489, 1.4) -- Should be allowed
  , (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPhi, deltaS) = CFTBound
      { bound = (singletScalarDefaultGaps nmax deltaPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalarDirty_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBoundWithFileTreatment
                     (keepAllFiles { sdpDirTreatment = RemoveFile })
                     defaultCFTBoundFiles . bound)
  [ -- (0.5181489, 1.4) -- Should be allowed
  -- ,
    (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPhi, deltaS) = CFTBound
      { bound = (singletScalarDefaultGaps nmax deltaPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalar_binary_search_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (deltaPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = CFTBound
        { bound = singletScalarDefaultGaps nmax deltaPhi
        , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
        , solverParams = (jumpFindingParams nmax) { precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 0
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 8
          }
        , threshold  = 1e-30
        , terminateTime = Nothing
        }
      }

boundsProgram "SingletScalar_twist_gap_2_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (deltaPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = CFTBound
        { bound = (singletScalarDefaultGaps nmax deltaPhi)
          { objective = SS.TwistGap 2 }
        , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
        , solverParams = (optimizationParams nmax) { precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 2
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 3
          , falsePoint = 3.5
          }
        , threshold  = 1e-10
        , terminateTime = Nothing
        }
      }

boundsProgram "SingletScalar_twist_gap_2_test_nmax8_to_18" =
  mapConcurrently_ search
  [ (8,  MPIJob 1 10, 1*hour, 768)
  , (10, MPIJob 1 16, 2*hour, 768)
  , (12, MPIJob 1 16, 4*hour, 768)
  , (14, MPIJob 1 32, 4*hour, 768)
  , (16, MPIJob 1 32, 8*hour, 832)
  , (18, MPIJob 1 32, 8*hour, 896)
  ]
  where
    deltaPhi = 0.5181489
    search (nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = CFTBound
        { bound = (singletScalarDefaultGaps nmax deltaPhi)
          { objective = SS.TwistGap 2 }
        , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
        , solverParams = (optimizationParams nmax) { precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 2
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { -- | Just under 2*deltaPhi + 2
            truePoint  = 3.036
            -- | Upper bound from nmax=6 computation
          , falsePoint = 3.043
          }
        , threshold  = 1e-10
        , terminateTime = Nothing
        }
      }

boundsProgram "SingletScalar_twist_gap_2_test_nmax22_26_30" =
  mapConcurrently_ search
  [ (22, MPIJob 2 32, 24*hour, 1216)
  , (26, MPIJob 2 32, 24*hour, 1280)
  , (30, MPIJob 4 32, 24*hour, 1440)
  ]
  where
    deltaPhi = 0.5181489
    search (nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = CFTBound
        { bound = (singletScalarDefaultGaps nmax deltaPhi)
          { objective = SS.TwistGap 2 }
        , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
        , solverParams = (optimizationParams nmax)
          { precision = prec
          , dualityGapThreshold = 1e-30
          }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 2
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { -- | Optimal bound
            truePoint  = 2*deltaPhi + 2
            -- | Upper bound from nmax=18 computation
          , falsePoint = 3.0363828125
          }
        , threshold  = 1e-10
        , terminateTime = Nothing
        }
      }


boundsProgram p = throwM (UnknownProgram p)
