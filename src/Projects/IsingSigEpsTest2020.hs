{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.IsingSigEpsTest2020 where

import           Bounds.IsingSigEps                          (ExternalDims (..),
                                                              IsingSigEps (..))
import qualified Bounds.IsingSigEps                          as Ising
import           Control.Concurrent                          (threadDelay)
import           Control.Monad                               (forM_, void, when)
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (asks, local)
import           Data.Aeson                                  (FromJSON, ToJSON)
import           Data.List.Split                             (chunksOf)
import qualified Data.Map.Strict                             as Map
import           Data.Proxy                                  (Proxy)
import           Data.Ratio                                  (approxRational)
import           Data.Reflection                             (reifyNat)
import           Data.Scientific                             (Scientific)
import qualified Data.Set                                    as Set
import           Data.Text                                   (Text)
import           Data.Typeable                               (Typeable)
import           Hyperion
import           Hyperion.Bootstrap.AffineTransform          (AffineTransform (..))
import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..),
                                                              CFTBoundFiles (..),
                                                              checkpointDir,
                                                              computeCFTBoundClean',
                                                              computeCFTBoundWithFileTreatment,
                                                              remoteComputeBound,
                                                              defaultCFTBoundFiles,
                                                              keepAllFiles,
                                                              keepOutAndCheckpoint,
                                                              outDir,
                                                              setFixedTimeLimit)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (blockParamsNmax,
                                                              jumpFindingParams,
                                                              optimizationParams,
                                                              sdpbParamsNmax,
                                                              spinsNmax)
import           Hyperion.Bootstrap.DelaunaySearch           (delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import           Hyperion.Bootstrap.OPESearch                (BilinearForms (..),
                                                              OPESearchConfig (..),
                                                              TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch                as OPE
import qualified Hyperion.Bootstrap.SDPDeriv                 as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.NewtonTrust     as NewtonTrust
import qualified Hyperion.Database                           as DB
import qualified Hyperion.Log                                as Log
import qualified Hyperion.Slurm                              as Slurm
import           Hyperion.Util                               (hour, minute)
import           Linear.V                                    (V)
import           Numeric.Rounded                             (Rounded,
                                                              RoundingMode (..))
import           Projects.Defaults                           (defaultBoundConfig,
                                                              defaultDelaunayConfig)
import qualified SDPB.Blocks.ScalarBlocks                    as SB
import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))
import           SDPB.Bounds.Spectrum                        (setGaps,
                                                              unitarySpectrum)
import           SDPB.Math.Linear                            (diagonalMatrix,
                                                              fromV, toV)
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB
import           System.Directory                            (createDirectoryIfMissing,
                                                              removePathForcibly)

sdpdIsingSigEps
  :: CFTBound Int IsingSigEps
  -> CFTBoundFiles
  -> CFTBound Int IsingSigEps
  -> CFTBoundFiles
  -> Job (SDPDeriv.Output Scientific)
sdpdIsingSigEps bound1 files1 bound2 files2 = do
  let prec = SDPB.precision (solverParams bound1)
  reifyNat (fromIntegral prec) $ \(_ :: Proxy p) -> do
    out :: SDPDeriv.Output (Rounded 'TowardZero p) <- SDPDeriv.sdpDerivBoundPair bound1 files1 bound2 files2
    pure (fmap realToFrac out)

remoteIsingSigEpsOPESearch
 :: TrackedMap Cluster (CFTBound Int IsingSigEps) FilePath
 -> TrackedMap Cluster (CFTBound Int IsingSigEps) (V 2 Rational)
 -> Rational
 -> Rational
 -> CFTBound Int IsingSigEps
 -> Cluster Bool
remoteIsingSigEpsOPESearch checkpointMap lambdaMap thetaMin thetaMax =
 OPE.remoteOPESearch (cPtr (static isingSigEpsOPESearchCfg)) checkpointMap lambdaMap initialBilinearForms
 where
   initialBilinearForms =
     BilinearForms 1e-16 [(Nothing, OPE.thetaIntervalFormApprox 1e-16 thetaMin thetaMax)]
   isingSigEpsOPESearchCfg =
     OPESearchConfig setLambda Ising.getExternalMat OPE.queryAllowedBoolFunction
   setLambda lambda cftBound = cftBound
     { bound = (bound cftBound)
       { objective = Ising.Feasibility (Just lambda) }
     }

remoteComputeIsingSigEpsBounds :: [CFTBound Int IsingSigEps] -> Cluster ()
remoteComputeIsingSigEpsBounds cftBounds = do
  -- Wait between 15 seconds and an hour. Hopefully this relieves
  -- pressure on the database when submitting a large number of
  -- jobs...
  -- t <- liftIO $ randomRIO (15, 3600)
  -- liftIO $ threadDelay $ t*1000*1000
  workDir <- newWorkDir (head cftBounds)
  jobTime <- asks (Slurm.time . clusterJobOptions)
  remoteEvalJob $
    static compute `ptrAp` cPure jobTime `cAp` cPure cftBounds `cAp` cPure workDir
  where
    computationsMap :: DB.KeyValMap (CFTBound Int IsingSigEps) SDPB.Output
    computationsMap = DB.KeyValMap "computations"
    compute jobTime cftBounds' workDir' = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams (head cftBounds'))
      forM_ (zip cftBounds' [1 :: Int ..]) $ \(cftBound,i) ->
        DB.lookup computationsMap cftBound >>= \case
          Just _  -> pure ()
          Nothing -> do
            Log.text $ "Computing bound " <> Log.showText i <> " of " <> Log.showText (length cftBounds') <> "."
            let boundFiles = defaultCFTBoundFiles workDir'
            result <- computeCFTBoundClean' (cftBound { solverParams = solverParams' }) boundFiles
            when (SDPB.isFinished result) $
              DB.insert computationsMap cftBound result
            liftIO $ mapM_ removePathForcibly [checkpointDir boundFiles, outDir boundFiles]

isingDimVector :: CFTBound prec IsingSigEps -> V 2 Rational
isingDimVector CFTBound{ bound = i } =
  toV (deltaSigma (externalDims i), deltaEps (externalDims i))

newIsingCheckpointMap
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (CFTBound Int IsingSigEps) FilePath)
newIsingCheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine isingDimVector mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "isingCheckpoints")

newIsingLambdaMap
  :: (FromJSON l, ToJSON l, Typeable l)
  => AffineTransform 2 Rational
  -> Maybe l
  -> Cluster (TrackedMap Cluster (CFTBound Int IsingSigEps) l)
newIsingLambdaMap affine mLambda =
 liftIO (OPE.affineLocalityMap affine isingDimVector mLambda) >>=
 OPE.mkPersistent (DB.KeyValMap "isingLambdas")

thetaVectorApprox :: Double -> Rational -> V 2 Rational
thetaVectorApprox res theta = toV (cos' theta, sin' theta)
  where
    cos' = (`approxRational` res) . cos . fromRational
    sin' = (`approxRational` res) . sin . fromRational

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

isingFeasibleDefaultGaps :: V 2 Rational -> V 2 Rational -> Int -> IsingSigEps
isingFeasibleDefaultGaps deltaExts lambda nmax = IsingSigEps
  { spectrum     = setGaps [ ((0, Ising.Z2Even), 3)
                           , ((0, Ising.Z2Odd),  3)
                           ] unitarySpectrum
  , objective    = Ising.Feasibility (Just lambda)
  , externalDims = Ising.ExternalDims {..}
  , blockParams  = blockParamsNmax nmax
  , spins        = spinsNmax nmax
  }
  where
    (deltaSigma, deltaEps) = fromV deltaExts

isingGFFNavigatorDefaultGaps :: V 2 Rational -> Maybe (V 2 Rational) -> Int -> IsingSigEps
isingGFFNavigatorDefaultGaps deltaExts lambda nmax = IsingSigEps
  { spectrum     = setGaps [ ((0, Ising.Z2Even), 3)
                           , ((0, Ising.Z2Odd),  3)
                           ] unitarySpectrum
  , objective = Ising.GFFNavigator lambda
  , externalDims = Ising.ExternalDims {..}
  , blockParams  = blockParamsNmax nmax
  , spins        = spinsNmax nmax
  }
  where
    (deltaSigma, deltaEps) = fromV deltaExts

isingAffineNmax6 :: AffineTransform 2 Rational
isingAffineNmax6 =  AffineTransform
  { affineShift  = toV (0.517, 1.395)
  , affineLinear = toV ( toV (0.005, 0.035)
                       , toV (0, 0.02)
                       )
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "IsingSigEpsAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (45*minute)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [ (toV (0.517075, 1.396575), thetaVectorApprox 1e-16 0.964)
  , (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
  ]
  where
    nmax = 6
    bound (deltaExts, lambda) = CFTBound
      { bound = isingFeasibleDefaultGaps deltaExts lambda nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEpsCT_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $ void $
  remoteComputeBound $
  bound (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
  where
    nmax = 6
    bound (deltaExts, lambda) = CFTBound
      { bound = (isingFeasibleDefaultGaps deltaExts lambda nmax)
        { objective  = Ising.StressTensorOPEBound (Just lambda) UpperBound }
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEpsAllowed_OPESearch_test_nmax6" =
 local (setJobType (MPIJob 1 4)) $ do
 checkpointMap <- newIsingCheckpointMap affine Nothing
 lambdaMap     <- newIsingLambdaMap affine Nothing
 mapConcurrently_ (remoteIsingSigEpsOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
   [ (toV (0.517075, 1.396575), OPE.thetaVectorApprox 1e-16 0.964)
   , (toV (0.518149, 1.412625), OPE.thetaVectorApprox 1e-16 1.0)
   ]
 where
   nmax = 6
   affine = isingAffineNmax6
   bound (deltaExts, lambda) = CFTBound
     { bound = isingFeasibleDefaultGaps deltaExts lambda nmax
     , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
     -- | It is very important to use jumpFindingParams when doing an OPESearch
     , solverParams = (jumpFindingParams nmax) { precision = 768 }
     , boundConfig = defaultBoundConfig
     }

boundsProgram "IsingSigEps_Island_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 4)) $ do
  checkpointMap <- newIsingCheckpointMap affine Nothing
  lambdaMap     <- newIsingLambdaMap affine Nothing
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteIsingSigEpsOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
  where
    nmax = 6
    affine = isingAffineNmax6
    bound deltaExts = CFTBound
      { bound = isingFeasibleDefaultGaps deltaExts lambdaInit nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    lambdaInit = OPE.thetaVectorApprox 1e-16 0.962
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = []
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "IsingSigEpsAllowed_test_nmax6_nersc" =
  local (setSlurmConstraint "haswell" . setSlurmPartition "debug") $
  boundsProgram "IsingSigEpsAllowed_test_nmax6"

boundsProgram "IsingSigEpsAllowed_test_nmax6_expanse" =
  local (setSlurmPartition "debug") $
  boundsProgram "IsingSigEpsAllowed_test_nmax6"

boundsProgram "IsingSigEps_GFFNavigator_test_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobTime (3*hour)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 50 $ do
  deltaSig <- [0.515, 0.5151 .. 0.525]
  deltaEps <- [1.37, 1.371 .. 1.47]
  pure (deltaSig, deltaEps, thetaSigEps)
  where
    nmax = 6
    thetaSigEps = 0.96926
    bound (deltaSig, deltaEps, th) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEps_GFFNavigator_test_theta_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 50 $ do
  let
    deltaEps = 1.412625
  deltaSig <- [0.515, 0.5151 .. 0.525]
  thetaSigEps <- [0.859,0.861 .. 1.059]
  pure (deltaSig, deltaEps, thetaSigEps)
  where
    nmax = 6
    bound (deltaSig, deltaEps, th) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 640 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "IsingSigEps_GFFNavigator_theta_eps_high_res_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 50 $ do
  let
    deltaSig = 0.5181489
  deltaEps <- [1.37, 1.371 .. 1.47]
  thetaSigEps <- [0.859,0.861 .. 1.059]
  pure (deltaSig, deltaEps, thetaSigEps)
  where
    nmax = 6
    bound (deltaSig, deltaEps, th) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEps_GFFNavigator_sig_eps_no_lambda_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (12*hour)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 80 extraPts
  where
    pts1 = (,) <$> [0.515, 0.5151 .. 0.525] <*> [1.37, 1.371 .. 1.47]
    pts2 = (,) <$> [0.51,  0.5101 .. 0.53]  <*> [1.3,  1.301 .. 1.5]
    extraPts = Set.toList $ Set.fromList pts2 `Set.difference` Set.fromList pts1
    nmax = 6
    bound (deltaSig, deltaEps) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) Nothing nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEps_GFFNavigator_test_nmax10" =
  local (setJobType (MPIJob 1 24) . setJobTime (48*hour)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 80 $ do
  deltaSig <- [0.515, 0.5151 .. 0.525]
  deltaEps <- [1.37, 1.371 .. 1.47]
  -- deltaSig <- [0.515, 0.516 .. 0.525]
  -- deltaEps <- [1.37, 1.38 .. 1.47]
  pure (deltaSig, deltaEps, thetaSigEps)
  where
    nmax = 10
    thetaSigEps = 0.96926
    bound (deltaSig, deltaEps, th) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEps_GFFNavigator_nmax6_sdp_derivative_test" =
  local (setJobType (MPIJob 1 8) . setJobTime (1*hour)) $ do
  results <- mapConcurrently myRemoteComputeBound bounds
  remoteEvalJob $
    static remoteDerivatives `ptrAp` cPure (zip bounds (map snd results))
  where
    bounds = do
      deltaSig <- 0.518 : [0.518 + 0.01*(1/2) ^^ n | n <- [0::Int .. 25] ]
      let deltaEps = 1.4
      pure $ bound (deltaSig, deltaEps)
    nmax = 6
    bound (deltaSig, deltaEps) = CFTBound
      { bound = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) Nothing nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax)
        { precision = 768
        , dualityGapThreshold = 1e-30
        , writeSolution = SDPB.allSolutionParts
        }
      , boundConfig = defaultBoundConfig
      }

    remoteDerivatives :: [(CFTBound Int IsingSigEps, CFTBoundFiles)] -> Job ()
    remoteDerivatives boundsAndFiles = do
      case boundsAndFiles of
        [] -> error "No bounds"
        (bound1, files1) : rest ->
          forM_ rest $ \(bound2, files2) -> do
          deriv <- sdpdIsingSigEps bound1 files1 bound2 files2
          Log.info "Computed derivative" (bound1, bound2, deriv)
          DB.insert (DB.KeyValMap "derivatives") (bound1, bound2) deriv

    myRemoteComputeBound :: CFTBound Int IsingSigEps -> Cluster (SDPB.Output, CFTBoundFiles)
    myRemoteComputeBound cftBound = do
      workDir <- newWorkDir cftBound
      liftIO $ createDirectoryIfMissing True workDir
      let files = defaultCFTBoundFiles workDir
      jobTime <- asks (Slurm.time . clusterJobOptions)
      result <- remoteEvalJob $
        static compute `ptrAp` cPure jobTime `cAp` cPure cftBound `cAp` cPure files
      Log.info "Computed" (cftBound, result)
      DB.insert (DB.KeyValMap "computations") cftBound result
      pure (result, files)
        where
          compute jobTime cftBound' files' = do
            solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams cftBound')
            _ <- computeCFTBoundWithFileTreatment
              keepAllFiles
              cftBound' { solverParams = solverParams' }
              files'
            computeCFTBoundWithFileTreatment
              keepAllFiles
              cftBound' { solverParams = SDPDeriv.setCentralPathParams 10 solverParams' }
              files'

-- The navigator appears to have too many places where the hessian is
-- discontinuous for this naive Newton search (with trust region) to
-- work. So far BFGS is the most robust method.
boundsProgram "IsingSigEps_GFFNavigator_Newton_nmax6_test" =
  local (setJobType (MPIJob 1 8) . setJobTime (2*hour)) $ do
  result :: (V 2 Rational, [SDPDeriv.Jet2 n (Rounded 'TowardZero 200)]) <-
    SDPDeriv.remoteNewtonTrustBound nsConfig bjConfig
  Log.info "Finished Newton search" result
  where
    trustNorm xWidth yWidth = diagonalMatrix $ toV (1/xWidth^(2::Int), 1/yWidth^(2::Int))
    nsConfig = NewtonTrust.defaultConfig $ trustNorm 0.001 0.01
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.512, 1.34)
      }
    mkBound :: V 2 Rational -> CFTBound Int IsingSigEps
    mkBound v = CFTBound
      { bound = isingGFFNavigatorDefaultGaps v Nothing nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax)
        { precision = 768
        , dualityGapThreshold = 1e-30
        }
      , boundConfig = defaultBoundConfig
      }
      where
        nmax = 6

boundsProgram "sleep" = do
  Log.text "Sleeping for 5 seconds"
  liftIO (threadDelay (5*1000*1000))

boundsProgram p = throwM (UnknownProgram p)
