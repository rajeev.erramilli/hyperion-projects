{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.GNYTest2020 where

import qualified Bounds.GNY                                  as GNY
import qualified Bounds.GNYN1                                as GNYN1
import           Control.Monad                               (void)
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.Reader                        (local)
import qualified Data.Map.Strict                             as Map
import           Data.Matrix.Static                          (Matrix)
import           Data.Text                                   (Text)
import           Hyperion
import           Hyperion.Bootstrap.AffineTransform          (AffineTransform (..))
import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..),
                                                              newCheckpointMap,
                                                              newLambdaMap,
                                                              remoteComputeBound,
                                                              remoteComputeBoundWithCheckpointMap)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (block3dParamsNmax,
                                                              gnyspinsNmax,
                                                              jumpFindingParams,
                                                              optimizationParams,
                                                              sdpbParamsNmax)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import           Hyperion.Bootstrap.OPESearch                (BilinearForms (..),
                                                              OPESearchConfig (..),
                                                              TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch                as OPE
import qualified Hyperion.Database                           as DB
import           Hyperion.Util                               (hour, minute)
import           Projects.Defaults                           (defaultBoundConfig,
                                                              defaultDelaunayConfig,
                                                              defaultQuadraticNetConfig)
import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))

import           Hyperion.Bootstrap.DelaunaySearch           (delaunaySearchRegionPersistent)
import           Linear.V                                    (V)
import qualified SDPB.Blocks.Blocks3d                        as B3d
import qualified SDPB.Blocks.Delta                           as Delta
import           SDPB.Bounds.Spectrum                        (addIsolated,
                                                              setGaps,
                                                              setTwistGap,
                                                              unitarySpectrum)
import qualified SDPB.Bounds.Spectrum                        as Spectrum
import           SDPB.Math.Linear.Literal                    (fromV, toM, toV)
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB

import qualified Data.Set                                    as Set
import qualified Projects.GNYData2020Chi35                   as GNYData
import qualified Projects.GNYN4Data                          as N4
import qualified Projects.GNYN8Data                          as N8

remoteGNYOPESearch
  :: TrackedMap Cluster (CFTBound Int GNY.GNY) FilePath
  -> TrackedMap Cluster (CFTBound Int GNY.GNY) (V 2 Rational)
  -> Rational
  -> Rational
  -> CFTBound Int GNY.GNY
  -> Cluster Bool
remoteGNYOPESearch checkpointMap lambdaMap thetaMin thetaMax =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    initialBilinearForms =
      BilinearForms 1e-16 [(Nothing, OPE.thetaIntervalFormApprox 1e-16 thetaMin thetaMax)]
    opeSearchCfg = 
      OPESearchConfig setLambda GNY.getEpsMat OPE.queryAllowedBoolFunction
    setLambda :: (V 2 Rational) -> CFTBound prec GNY.GNY -> CFTBound prec GNY.GNY
    setLambda lambda cftBound = case (GNY.objective $ bound cftBound) of
      GNY.FeasibilityEps deltaEps _ ->
        cftBound
        { bound = (bound cftBound)
          { GNY.objective = GNY.FeasibilityEps deltaEps lambda}
        }
      _ -> error "can't do OPE search without FeasibilityEps objective"

remoteGNYExtEpsOPESearch
  :: TrackedMap Cluster (CFTBound Int GNY.GNY) FilePath
  -> TrackedMap Cluster (CFTBound Int GNY.GNY) (V 3 Rational)
  -> BilinearForms 3
  -> CFTBound Int GNY.GNY
  -> Cluster Bool
remoteGNYExtEpsOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg =
      OPESearchConfig setLambda GNY.getExtEpsMat $ OPE.queryAllowedMixed qmConfig
    setLambda :: (V 3 Rational) -> CFTBound prec GNY.GNY -> CFTBound prec GNY.GNY
    setLambda lambda cftBound = case (GNY.objective $ bound cftBound) of
      GNY.FeasibilityExtEps deltaEps _ ->
        cftBound
        { bound = (bound cftBound)
          { GNY.objective = GNY.FeasibilityExtEps deltaEps lambda}
        }
      _ -> error "can't do OPE search without FeasibilityEps objective"
    qmConfig = OPE.QueryMixedConfig
      { qmQuadraticNetConfig = defaultQuadraticNetConfig
      , qmDescentConfig      = OPE.defaultDescentConfig
      , qmResolution         = 1e-16
      , qmPrecision          = 256
      , qmHessianLineSteps   = 200
      , qmHessianLineAverage = 10
      }

-- | Convert a 'CFTBound prec GNY' to a 2-dimensional vector given by
--
-- (deltaPsi, deltaSig)
--
-- It is important that the ordering convention for 2-dimensional
-- vectors of dimensions be kept consistent in all computations.
deltaExtToV :: CFTBound prec GNY.GNY -> V 2 Rational
deltaExtToV CFTBound { bound = GNY.GNY { externalDims = eds } } =
  toV (GNY.deltaPsi eds, GNY.deltaSig eds)

-- | Convert a 'CFTBound prec GNY' to a 3-dimensional vector given by
--
-- (deltaPsi, deltaSig, deltaEps)
--
-- This function assumes the epsilon channel contains at least one
-- isolated operator, and defines deltaEps as the lowest such
-- operator. The function throws an error otherwise.  It is important
-- to keep the ordering convention consistent in computations that use
-- this function.
deltaExtEpsToV :: CFTBound prec GNY.GNY -> V 3 Rational
deltaExtEpsToV CFTBound { bound = GNY.GNY { externalDims = eds, spectrum = s } } =
  toV (GNY.deltaPsi eds, GNY.deltaSig eds, deltaEps)
  where
    deltaEps = case Set.toAscList (Spectrum.deltaIsolated s epsChannel) of
      Delta.Fixed d : _ -> d
      _ -> error $ "Spectrum does not contain an isolated operator in the epsilon channel: " ++ show s
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet

gnyFeasibleDefaultGaps :: V 2 Rational -> Rational -> Int -> GNY.GNY
gnyFeasibleDefaultGaps deltaExts ngroup nmax = GNY.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setGaps [ (psiChannel, 2)
                           , (chiChannel, 3.5)
                           , (sigChannel, 3)
                           , (epsChannel, 3)
                           , (sigTChannel, 2)
                           -- , (epsTChannel, 3)
                           ] $ addIsolated epsChannel 1.9 unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNY.Feasibility
  , externalDims = GNY.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = GNY.ChannelType (1/2) B3d.ParityEven GNY.ONVector
    sigChannel  = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSinglet
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet
    chiChannel  = GNY.ChannelType (1/2) B3d.ParityOdd GNY.ONVector
    sigTChannel = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSymTensor
    --epsTChannel = GNY.ChannelType 0 B3d.ParityEven GNY.ONSymTensor
    (deltaPsi,deltaSig) = fromV deltaExts

gny3dSearchDefaultGaps :: V 3 Rational -> Rational -> Int -> GNY.GNY
gny3dSearchDefaultGaps deltaV ngroup nmax = GNY.GNY
  { spectrum     = setGaps [ (psiChannel, 2)
                           , (chiChannel, 3.5)
                           , (sigChannel, 3)
                           , (epsChannel, 3)
                           , (sigTChannel, 2)
                           ] $ addIsolated epsChannel deltaEps unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNY.Feasibility
  , externalDims = GNY.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = GNY.ChannelType (1/2) B3d.ParityEven GNY.ONVector
    sigChannel  = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSinglet
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet
    chiChannel  = GNY.ChannelType (1/2) B3d.ParityOdd GNY.ONVector
    sigTChannel = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSymTensor
    --epsTChannel = GNY.ChannelType 0 B3d.ParityEven GNY.ONSymTensor
    (deltaPsi,deltaSig,deltaEps) = fromV deltaV

gnyn1FeasibleDefaultGaps :: V 2 Rational -> Int -> GNYN1.GNY
gnyn1FeasibleDefaultGaps deltaExts nmax = GNYN1.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setGaps [ (phiChannel, 3)
                           , (psiChannel, 2)
                           ] unitarySpectrum
  , objective    = GNYN1.Feasibility
  , externalDims = GNYN1.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    phiChannel = GNYN1.ChannelType (1/2) B3d.ParityEven
    psiChannel = GNYN1.ChannelType 0 B3d.ParityOdd
    (deltaSig,deltaPsi) = fromV deltaExts

superIsingFeasibleDefaultGaps :: V 2 Rational -> Int -> GNYN1.GNY
superIsingFeasibleDefaultGaps deltaExts nmax = GNYN1.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setGaps [ (sigChannel, 2.8)
                           , (psiChannel, 2)
                           , (epsChannel, 1)
                           , (chiChannel, 3.5)
                           ] unitarySpectrum
  , objective    = GNYN1.Feasibility
  , externalDims = GNYN1.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    sigChannel = GNYN1.ChannelType 0 B3d.ParityOdd
    psiChannel = GNYN1.ChannelType (1/2) B3d.ParityEven
    epsChannel = GNYN1.ChannelType 0 B3d.ParityEven
    chiChannel = GNYN1.ChannelType (1/2) B3d.ParityOdd
    (deltaPsi,deltaSig) = fromV deltaExts

gnyEpsFeasibleDefaultGaps :: V 3 Rational -> Rational -> Int -> GNY.GNY
gnyEpsFeasibleDefaultGaps deltaExts ngroup nmax = GNY.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setTwistGap 1e-6 $ setGaps [ (psiChannel, 2)
                           , (chiChannel, 3.5)
                           , (sigChannel, 3)
                           , (epsChannel, 3)
                           , (sigTChannel, 2)
                           -- , (epsTChannel, 3)
                           ] $ addIsolated epsChannel deltaEps unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNY.Feasibility
  , externalDims = GNY.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = GNY.ChannelType (1/2) B3d.ParityEven GNY.ONVector
    sigChannel  = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSinglet
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet
    chiChannel  = GNY.ChannelType (1/2) B3d.ParityOdd GNY.ONVector
    sigTChannel = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSymTensor
    --epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
    (deltaPsi,deltaSig, deltaEps) = fromV deltaExts

gnyFeasibleEpsDefaultGaps :: V 3 Rational -> (V 2 Rational) -> Rational -> Int -> GNY.GNY
gnyFeasibleEpsDefaultGaps deltaExts lambda ngroup nmax = GNY.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setTwistGap 1e-6 $ setGaps [ (psiChannel, 2)
                           , (chiChannel, 3.5)
                           , (sigChannel, 3)
                           , (epsChannel, 3)
                           , (sigTChannel, 2)
                           -- , (epsTChannel, 3)
                           ]  unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNY.FeasibilityEps deltaEps lambda
  , externalDims = GNY.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = GNY.ChannelType (1/2) B3d.ParityEven GNY.ONVector
    sigChannel  = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSinglet
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet
    chiChannel  = GNY.ChannelType (1/2) B3d.ParityOdd GNY.ONVector
    sigTChannel = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSymTensor
    --epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
    (deltaPsi,deltaSig, deltaEps) = fromV deltaExts

gnyFeasibleExtEpsDefaultGaps :: V 3 Rational -> (V 3 Rational) -> Rational -> Int -> GNY.GNY
gnyFeasibleExtEpsDefaultGaps deltaExts lambda ngroup nmax = GNY.GNY
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = setTwistGap 1e-6 $ setGaps [ (psiChannel, 2)
                           , (chiChannel, 3.5)
                           , (sigChannel, 3)
                           , (epsChannel, 3)
                           , (sigTChannel, 2)
                           -- , (epsTChannel, 3)
                           ]  unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNY.FeasibilityExtEps deltaEps lambda
  , externalDims = GNY.ExternalDims {..}
  , blockParams  = block3dParamsNmax nmax
  , spins        = gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = GNY.ChannelType (1/2) B3d.ParityEven GNY.ONVector
    sigChannel  = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSinglet
    epsChannel  = GNY.ChannelType 0 B3d.ParityEven GNY.ONSinglet
    chiChannel  = GNY.ChannelType (1/2) B3d.ParityOdd GNY.ONVector
    sigTChannel = GNY.ChannelType 0 B3d.ParityOdd GNY.ONSymTensor
    --epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
    (deltaPsi,deltaSig, deltaEps) = fromV deltaExts


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

-- | (deltaPsi, deltaPhi)
superIsingAffineNmax6 :: AffineTransform 2 Rational
superIsingAffineNmax6 = AffineTransform
  { affineShift  = toV ( 1.075, 0.75 )
  , affineLinear = toV ( toV (0.025, 0)
                       , toV (0, 0.25)
                       )
  }

gny4AffineNmax6 :: AffineTransform 2 Rational
gny4AffineNmax6 = AffineTransform
  { affineShift  = toV ( 1.05, 0.75 )
  , affineLinear = toV ( toV (0.05, 0)
                       , toV (0, 0.25)
                       )
  }

-- gny4Affine3dNmax6 :: AffineTransform 3 Rational
-- gny4Affine3dNmax6 = AffineTransform
--   { affineShift  = toV ( 1.05, 0.75, 1.75)
--   , affineLinear = toV ( toV (0.05, 0, 0)
--                        , toV (0, 0.25, 0)
--                        , toV (0, 0, 1.25)
--                        )
--   }

gny4Affine3dNmax6 :: AffineTransform 3 Rational
gny4Affine3dNmax6 = AffineTransform
  { affineShift  = toV ( 1.0456, 0.742559, 1.50274)
  , affineLinear = toV ( toV (-0.00289182, 0.0286252, 0.527533)
                       , toV (-0.000864904, -0.0227698, 0.00123081)
                       , toV (0.00281221, -0.000105677, 0.0000211502)
                       )
  }


boundsProgram :: Text -> Cluster ()

boundsProgram "GNYN1Allowed_test_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [ toV (0.45962, 1.15272) -- Large N expansion, N=2
  , toV (0.72981, 1.05507) -- Large N expansion, N=4
  ]
  where
    nmax = 6
    bound deltaExts = CFTBound
      { bound = gnyn1FeasibleDefaultGaps deltaExts nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

-----------------------
------ N=2 2d search --
-----------------------

boundsProgram "GNYAllowed_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (2*hour)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [toV (1.07,0.645)
  , toV (1.07,0.63)
  ,toV (1.06, 0.6)
  ]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYCTBound_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $ void $
  remoteComputeBound $
  bound (toV (0.518149, 1.412625))
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = (gnyFeasibleDefaultGaps deltaExts ngroup nmax)
        { GNY.objective  = GNY.StressTensorOPEBound UpperBound }
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYExternalBound_test_ngroup2_nmax6" =
  --local (setJobType (MPIJob 1 4)) $ do void $ delaunaySearchRegionPersistent delaunaySearchPoints (defaultDelaunayConfig 1 2) affine Map.empty f
  local (setJobType (MPIJob 1 6) . setJobTime (8*hour)) $ do void $ delaunaySearchRegionPersistent delaunaySearchPoints (defaultDelaunayConfig 10 100) affine (Map.singleton (toV (0.62,1.06)) (Just True)) f
  where
    nmax = 6
    ngroup = 2
    affine = AffineTransform
      { affineShift  = toV (0.55, 1.05)
      , affineLinear = toV ( toV (0.35,0)
                           , toV (0, 0.05)
                           )
      }
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound (bound deltaExts))
    bound deltaExts = CFTBound
      { bound = (gnyFeasibleDefaultGaps deltaExts ngroup nmax)
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_Island_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (1*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2AffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV [(1.07,0.635)]
        initialAllowed = [toV (1.07,0.645)]

-------------------
-- N=2 3d search --
-------------------

boundsProgram "GNY_3d_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV GNYData.epsN2nmax6PrimalPoints --[(1.06804, 0.635651, 1.47334)]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNY_3d_test_ngroup2_nmax8" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV $ GNYData.n2nmax8DualPoints
  where
    nmax = 8
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_3d_test_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV $ GNYData.n2nmax10TestPoints
  where
    nmax = 10
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_3d_test_ngroup2_nmax12" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV $ GNYData.n2nmax12DualPoints
  where
    nmax = 12
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNY_3d_island_ngroup2_nmax6" =
 local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 800
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax6DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax6PrimalPoints



boundsProgram "GNY_3d_island_ngroup2_nmax8" =
 local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax8
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 600
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax8DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax8PrimalPoints

boundsProgram "GNY_3d_island_ngroup2_nmax10" =
 local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 10
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax10
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 5 50
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax10DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax10PrimalPoints


boundsProgram "GNY_3d_island_ngroup2_nmax6_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 1 2
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax6DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax6PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

boundsProgram "GNY_3d_island_ngroup2_nmax8_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax8
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 1 2
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax8DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax8PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

boundsProgram "GNY_3d_island_ngroup2_nmax10_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 24) . setJobMemory "72G" . setJobTime (8*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 10
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax10
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 100
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax10DualPoints
        initialAllowed = fmap toV $ GNYData.n2nmax10PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing  --"/central/groups/dssimmon/aike/test/data/2020-11/KJrLH/Object_uwnS1eRBtHdv-MEYw0V9aB0Jy_TTwoj-5qmLNVakeFg/ck"

boundsProgram "GNY_3d_island_ngroup2_nmax12_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 18) . setJobMemory "96G" . setJobTime (24*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 12
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax12
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 32 600
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.n2nmax12DualPoints
        initialAllowed = []--fmap toV $ GNYData.n2nmax12PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing




----------------
-- N=4 search --
----------------


boundsProgram "GNY_Island_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 4
    affine = gny4AffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV [(1.04,0.8)]
        initialAllowed = [toV (1.044,0.77)]

boundsProgram "GNY_3d_island_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 7) . setJobMemory "20G" . setJobTime (1*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 4
    affine = gny4Affine3dNmax6
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 2000
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV []
        initialAllowed = fmap toV [(1.044,0.77,1.8)]

boundsProgram "GNY_3d_island_ngroup4_nmax6_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 4
    affine = gny4Affine3dNmax6
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 2000
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV []
        initialAllowed = fmap toV [(1.044,0.77,1.8)]
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

boundsProgram "GNY_3d_island_ngroup4_nmax8" =
  local (setJobType (MPIJob 1 18) . setJobMemory "40G" . setJobTime (4*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 4
    affine = gny4Affine3dNmax6
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 26 1000
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N4.nmax6DualPoints
        initialAllowed = fmap toV [(9341401777594260840536377/8947306799824896000000000,214138321200063753560017463/286313817594396672000000000,2367555402948089231516369081/1431569087971983360000000000)]

boundsProgram "GNY_3d_island_ngroup4_nmax10" =
  local (setJobType (MPIJob 1 18) . setJobMemory "80G" . setJobTime (8*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 10
    ngroup = 4
    affine = gny4Affine3dNmax6
    -- f :: V 3 Rational -> Cluster Bool
    -- f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 32 600
    mInitialCheckpoint = Nothing
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N4.nmax8DualPoints
        initialAllowed = N4.nmax8DualMean

boundsProgram "GNY_3d_island_ngroup4_nmax12" =
  local (setJobType (MPIJob 1 18) . setJobMemory "80G" . setJobTime (24*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 12
    ngroup = 4
    affine = N4.nmax10affine3d
    -- f :: V 3 Rational -> Cluster Bool
    -- f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 32 600
    mInitialCheckpoint = Nothing
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N4.nmax10DualPoints
        initialAllowed = []

boundsProgram "GNY_3d_island_ngroup4_nmax10_chigap3.5" =
  local (setJobType (MPIJob 1 36) . setJobMemory "120G" . setJobTime (12*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 10
    ngroup = 4
    affine = N4.nmax10affine3d
    -- f :: V 3 Rational -> Cluster Bool
    -- f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 400
    mInitialCheckpoint = Nothing
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N4.nmax10DualPoints
        initialAllowed = []

boundsProgram "GNY_3d_island_ngroup4_nmax12_chigap3.5" =
  local (setJobType (MPIJob 1 36) . setJobMemory "350G" . setJobTime (12*hour)) $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 12
    ngroup = 4
    affine = N4.nmax12affine3d
    -- f :: V 3 Rational -> Cluster Bool
    -- f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaV)
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 400
    mInitialCheckpoint = Nothing
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N4.nmax12DualPoints
        initialAllowed = []


boundsProgram "GNY_Island_ngroup4_nmax8" =
  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (2*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 4
    affine = gny4AffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $
          [(409/400, 3/4), (419/400, 5/8), (419/400, 7/8), (1717/1600, 21/32), (26/25, 23/32), (26/25, 25/32),
          (417/400, 3/4), (13417/12800, 175/256), (13907/12800, 159/256), (27019/25600, 445/512),
          (13421/12800, 213/256), (53773/51200, 691/1024), (667/640, 189/256), (13591/12800, 245/256),
          (107763/102400, 1749/2048), (667/640, 195/256), (444069/409600, 5265/8192), (439919/409600, 5423/8192),
          (213561/204800, 3011/4096), (53749/51200, 53/64), (53583/51200, 829/1024), (106737/102400, 1485/2048),
          (887183/819200, 10707/16384), (1779077/1638400, 21233/32768), (106761/102400, 6211/8192),
          (1766093/1638400, 21529/32768), (108317/102400, 339/512), (112077/102400, 1245/2048),
          (225041/204800, 2463/4096), (108349/102400, 1859/2048), (214833/204800, 841/1024),
          (85403/81920, 5871/8192), (1429953/1310720, 84529/131072), (34423/32768, 13677/16384),
          (213807/204800, 2893/4096), (3449163/3276800, 55701/65536), (871923/819200, 32283/32768),
          (6896029/6553600, 86631/131072), (2844053/2621440, 171461/262144), (697303/655360, 124831/131072),
          (1737549/1638400, 59879/65536), (173461/163840, 14663/16384), (7078993/6553600, 86413/131072),
          (28685061/26214400, 335985/524288), (853501/819200, 12811/16384), (3507567/3276800, 43437/65536),
          (28505817/26214400, 343541/524288), (3603713/3276800, 39327/65536), (106883/102400, 811/1024),
          (13791067/13107200, 221701/262144), (1380123/1310720, 87621/131072),
          (115135429/104857600, 1333169/2097152), (27664593/26214400, 349571/524288),
          (13887039/13107200, 174605/262144), (6964789/6553600, 245791/262144),
          (114733593/104857600, 1361205/2097152), (341707/327680, 25633/32768),
          (18441389/16777216, 5319313/8388608), (230230021/209715200, 2702193/4194304),
          (2777197/2621440, 473535/524288), (2741027/2621440, 178921/262144), (5581587/5242880, 1013611/1048576),
          (866271/819200, 10919/16384), (27568087/26214400, 354357/524288),
          (919292983/838860800, 10921435/16777216), (1843816687/1677721600, 21634419/33554432),
          (457561549/419430400, 5471257/8388608), (115312691/104857600, 1317549/2097152),
          (13808413/13107200, 224403/262144), (11532699/10485760, 635737/1048576), (174789/163840, 32637/32768),
          (227740279/209715200, 2738203/4194304), (13711891/13107200, 180613/262144),
          (3684856907/3355443200, 43551647/67108864), (110440707/104857600, 1410681/2097152),
          (55861347/52428800, 2047887/2097152), (27846429/26214400, 974439/1048576),
          (6969181/6553600, 87225/131072), (230619257/209715200, 2694183/4194304),
          (223354153/209715200, 8073533/8388608), (92257093/83886080, 5367879/8388608),
          (563657/524288, 173165/262144), (1831714673/1677721600, 21859629/33554432),
          (27412063/26214400, 363113/524288), (28244177/26214400, 346109/524288),
         (110656503/104857600, 1862041/2097152)]
        initialAllowed = [toV (1.044,0.77)]

boundsProgram "GNY_test_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (1*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(1.044,0.77,1+x/10) | x <-[0..10]]
  where
    nmax = 6
    ngroup = 4
    bound deltaExts = CFTBound
      { bound = gny3dSearchDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }



boundsProgram "SuperIsing_Island_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    affine = superIsingAffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = superIsingFeasibleDefaultGaps deltaExts nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = []
        initialAllowed = [toV (1.085,0.6)]

boundsProgram "BigJumpSearch_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    affine = superIsingAffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeBound $ bound deltaExts)
    bound deltaExts = CFTBound
      { bound = superIsingFeasibleDefaultGaps deltaExts nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 400
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = []
        initialAllowed = [toV (1.085,0.6)]

--------------
-- N=2 OPE ---
--------------

boundsProgram "GNY_OPEtest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(1.069,0.65,1.75)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    lambda :: (V 2 Rational)
    lambda = OPE.thetaVectorApprox 1e-16 1.0
    bound deltaExts = CFTBound
      { bound = gnyFeasibleEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_OPElesstest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(1.069,0.65,1.75)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = CFTBound
      { bound = gnyEpsFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768}
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_OPE_LambdaTest_ngroup2_nmax4" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour) . setSlurmPartition "scavenge") $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) [0,0.1..6.2]
  where
    nmax = 4
    ngroup = 2
    -- mLambda :: Maybe (V 4 Rational)
    -- mLambda = Just $ toV (0,1,1,1)
    deltaExts = toV (1.069,0.65,1.75)
    bound theta = CFTBound
      { bound = gnyFeasibleEpsDefaultGaps deltaExts (OPE.thetaVectorApprox 1e-16 theta) ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768}
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_OPESearchTest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (3*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- newLambdaMap affine deltaExtEpsToV Nothing
  mapConcurrently_ (remoteGNYOPESearch checkpointMap lambdaMap (-1.57) 1.57 . bound) $
     fmap toV [(1.069,0.65,1.66), (1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    lambda = OPE.thetaVectorApprox 1e-16 0.0
    bound deltaExts = CFTBound
      { bound = gnyFeasibleEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_OPESearchTest_ngroup2_nmax4" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour) . setSlurmPartition "scavenge") $ void $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- newLambdaMap affine deltaExtEpsToV Nothing
  mapConcurrently_ (remoteGNYOPESearch checkpointMap lambdaMap (-1.5) 1.5 . bound) $
     fmap toV [(x,y,z) | x <- [1.068,1.069..1.074], y <- [0.63,0.635..0.66], z <- [1.65,1.66..1.8]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 4
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    lambda = OPE.thetaVectorApprox 1e-16 0.0
    bound deltaExts = CFTBound
      { bound = gnyFeasibleEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_OPESearchTest_ngroup2_nmax8" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- newLambdaMap affine deltaExtEpsToV Nothing
  mapConcurrently_ (remoteGNYOPESearch checkpointMap lambdaMap (-1.5) 1.5 . bound) $
     fmap toV [(1.069,0.65,1.75)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    lambda = OPE.thetaVectorApprox 1e-16 0.0
    bound deltaExts = CFTBound
      { bound = gnyFeasibleEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_ExtEps_OPEtest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(1.069,0.65,1.75)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    lambda :: V 3 Rational
    lambda = toV (1,0.238476, 0.971148)
    bound deltaExts = CFTBound
      { bound = gnyFeasibleExtEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_ExtEps_OPESearchTest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- newLambdaMap affine deltaExtEpsToV Nothing
  mapConcurrently_ (remoteGNYExtEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [ (1.069,0.65,1.66)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    bilinearMat :: Matrix 3 3 Rational
    bilinearMat = toM
      ( (1,0,0)
      , (0,1,0)
      , (0,0,-100)
      )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]

    lambda :: V 3 Rational
    lambda = toV (1,0.238476, 0.971148)
    bound deltaExts = CFTBound
      { bound = gnyFeasibleExtEpsDefaultGaps deltaExts lambda ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_grid_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(x,y) | x <- [1.020,1.0202..1.022], y <- [0.8,0.81..0.9]]
  where
    nmax = 6
    ngroup = 8
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768}
      , boundConfig = defaultBoundConfig
      }

----------------
-- N=8 search --
----------------


boundsProgram "GNY_Island_ngroup8_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobMemory "50G" . setJobTime (1*hour) . setSlurmPartition "shared") $ do
  checkpointMap <- newCheckpointMap affine deltaExtToV Nothing
  let
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible $ remoteComputeBoundWithCheckpointMap checkpointMap $ bound deltaExts
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 8
    affine = N8.affine2d
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N8.nmax6dual
        initialAllowed = N8.nmax6primal

boundsProgram "GNY_grid_ngroup8_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (remoteComputeBound . bound) $
     fmap toV [(x,y) | x <- [1.020,1.0202..1.022], y <- [0.8,0.81..0.9]]
  where
    nmax = 6
    ngroup = 8
    bound deltaExts = CFTBound
      { bound = gnyFeasibleDefaultGaps deltaExts ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (jumpFindingParams nmax) { precision = 768}
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNY_3d_Island_ngroup8_nmax6" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 4) . setJobMemory "20G" . setJobTime (1*hour) . setSlurmPartition "shared") $ do
  checkpointMap <- newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      remoteComputeBoundWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 8
    affine = N8.nmax6affine3d
    bound deltaV = CFTBound
      { bound = gny3dSearchDefaultGaps deltaV ngroup nmax
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 30 1000
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = N8.nmax6dual3d
        initialAllowed = N8.nmax6primal3d
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

boundsProgram p = throwM (UnknownProgram p)



