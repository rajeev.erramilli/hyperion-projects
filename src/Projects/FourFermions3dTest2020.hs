{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.FourFermions3dTest2020 where

import           Bounds.FourFermions3d                       (FourFermions3d (..))
import qualified Bounds.FourFermions3d                       as FF
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.Reader                        (local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import qualified Data.Set                                    as Set
import           Data.Text                                   (Text)
import           GHC.Generics                                (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                              Bracket (..))
import           Hyperion.Bootstrap.CFTBound                 (BinarySearchBound (..),
                                                              CFTBound (..),
                                                              defaultResultBoolClosure,
                                                              remoteBinarySearchBound,
                                                              remoteComputeBound)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (block3dParamsNmax,
                                                              jumpFindingParams,
                                                              optimizationParams,
                                                              sdpbParamsNmax,
                                                              spinsNmax)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import           Hyperion.Util                               (hour, minute)
import           Projects.Defaults                           (defaultBoundConfig)
import qualified SDPB.Blocks.Blocks3d                        as B3d
import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))
import           SDPB.Bounds.Spectrum                        (setGap,
                                                              setTwistGap,
                                                              unitarySpectrum)
import           SDPB.Solver                                 (Params (..))

data FourFermionBinarySearch = FourFermionBinarySearch
  { ffbs_bound     :: CFTBound Int FourFermions3d
  , ffbs_config    :: BinarySearchConfig Rational
  , ffbs_gapSector :: (Int, B3d.Parity)
  } deriving (Show, Generic, Binary, ToJSON)

remoteFourFermions3dBinarySearch :: FourFermionBinarySearch -> Cluster (Bracket Rational)
remoteFourFermions3dBinarySearch ffbs =
  remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ffbs_bound ffbs) `cAp` cPure (ffbs_gapSector ffbs)
  , bsConfig            = ffbs_config ffbs
  , bsResultBoolClosure = defaultResultBoolClosure
  }
  where
    mkBound cftBound gapSector gap =
      cftBound { bound = (bound cftBound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (bound cftBound))

fourFermionsDefaultGaps :: Int -> Rational -> FourFermions3d
fourFermionsDefaultGaps nmax dPsi = FourFermions3d
  { spectrum     = setTwistGap 1e-6 $
                   unitarySpectrum
  , objective    = FF.Feasibility
  , blockParams  = block3dParamsNmax nmax
  , spins        = spinsNmax nmax
  , deltaPsi     = dPsi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "FourFermions3dAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [ (1.1, 3.780) -- Should be allowed
  , (1.1, 3.782) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPsi, deltaS) = CFTBound
      { bound = (fourFermionsDefaultGaps nmax deltaPsi)
        { spectrum     = setGap (0,B3d.ParityEven) deltaS $
                         setTwistGap 1e-6 $
                         unitarySpectrum
        }
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "FourFermions3dAllowed_test_nmax6_nersc" =
  local (setSlurmConstraint "haswell" . setSlurmPartition "debug") $
  boundsProgram "FourFermions3dAllowed_test_nmax6"

boundsProgram "FourFermions3d_binary_search_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteFourFermions3dBinarySearch . search)
  [ 1.1 ]
  where
    nmax = 6
    search deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = fourFermionsDefaultGaps nmax deltaPsi
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 8
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteFourFermions3dBinarySearch . search)
  [ 1.02, 1.04 .. 1.7 ]
  where
    nmax = 6
    search deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = fourFermionsDefaultGaps nmax deltaPsi
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 8
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteFourFermions3dBinarySearch . search)
  [ 1.02, 1.04 .. 1.7 ]
  where
    nmax = 6
    search deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = fourFermionsDefaultGaps nmax deltaPsi
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3dCTBound_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBound . bound)
  [ 1.1
  ]
  where
    nmax = 6
    bound deltaPsi = CFTBound
      { bound = (fourFermionsDefaultGaps nmax deltaPsi)
        { objective = FF.StressTensorOPEBound UpperBound }
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "FourFermions3d_binary_search_plot_nmax14" =
  local (setJobType (MPIJob 2 32) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0]
  deltaPsi    <- Set.toList $ Set.fromList $
    [1.01, 1.02 .. 1.7] ++
    [1.27, 1.272 .. 1.3] -- Extra detail near the kink
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 8
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_nmax18" =
  local (setJobType (MPIJob 3 32) . setJobTime (12*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0]
  deltaPsi    <- Set.toList $ Set.fromList $
    [1.01, 1.02 .. 1.7] ++
    [1.27, 1.272 .. 1.3] -- Extra detail near the kink
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 18
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 896 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 8
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2]
  deltaPsi    <- [1.01, 1.02 .. 1.7]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_larger_gaps_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [0.4, 0.6, 0.8, 1.0]
  deltaPsi    <- [1.01, 1.02 .. 1.7]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_near_jump_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2]
  deltaPsi    <- [1.2805, 1.2810 .. 1.2895]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_near_jump2_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2]
  deltaPsi    <- [1.28555, 1.28560 .. 1.28595]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_near_jump3_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau]
  deltaPsi    <- [1.2855025, 1.285505 .. 1.2855475]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-4
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_more_gaps_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [0.25, 0.3, 0.35]
  deltaPsi    <- [1.01, 1.02 .. 1.7] ++ [1.2805, 1.2810 .. 1.2895]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_parity_odd_smoothing_nmax14" =
  local (setJobType (MPIJob 2 28) . setJobTime (8*hour)) $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [0.2, 0.25 .. 2.4]
  deltaPsi    <- [1.285, 1.2875 .. 1.36]
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 14
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityOdd)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram "FourFermions3dAllowed_test_nmax6_expanse" =
  local (setSlurmPartition "shared") $
  boundsProgram "FourFermions3dAllowed_test_nmax6"

boundsProgram "FourFermions3d_binary_search_plot_nmax18_expanse" =
  local (setJobType (MPIJob 1 128) . setJobTime (12*hour) . setJobMemory "0") $
  mapConcurrently_ remoteFourFermions3dBinarySearch $ do
  spin1oddGap <- map (2+) [dTau, 0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0]
  deltaPsi    <- Set.toList $ Set.fromList $
    [1.01, 1.02 .. 1.7] ++
    [1.27, 1.272 .. 1.3] -- Extra detail near the kink
  pure $ search spin1oddGap deltaPsi
  where
    dTau = 1e-6
    nmax = 18
    search spin1oddGap deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = (fourFermionsDefaultGaps nmax deltaPsi)
          { spectrum =
              setGap (1, B3d.ParityOdd) spin1oddGap $
              setTwistGap dTau $
              unitarySpectrum
          }
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax)
          { precision = 896
          }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 2*deltaPsi
          , falsePoint = 8
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram p = throwM (UnknownProgram p)
