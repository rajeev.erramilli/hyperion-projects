{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.SingletScalarBlocks3d2020 where

import           Bounds.SingletScalarBlocks3d                (SingletScalar (..))
import qualified Bounds.SingletScalarBlocks3d                as SS
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Proxy                                  (Proxy (..))
import           Data.Scientific                             (Scientific)
import           Data.Text                                   (Text)
import           Data.Vector                                 (Vector)
import           GHC.Generics                                (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                              Bracket (..))
import           Hyperion.Bootstrap.CFTBound                 (BinarySearchBound (..),
                                                              CFTBound (..),
                                                              CFTBoundFiles (..),
                                                              getBoundObject,
                                                              reifyPrecisionWithFetchContext,
                                                              remoteBinarySearchBound,
                                                              remoteComputeBound,
                                                              toSDP)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (block3dParamsNmax,
                                                              blockParamsNmax,
                                                              jumpFindingParams,
                                                              spinsNmax)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import qualified Hyperion.Database                           as DB
import qualified Hyperion.Log                                as Log
import           Hyperion.Util                               (minute)
import           Numeric.Rounded                             (Rounded,
                                                              RoundingMode (..))
import           Projects.Defaults                           (defaultBoundConfig)
import qualified SDPB.Blocks.ScalarBlocks                    as SB
import           SDPB.Bounds.Spectrum                        (setGap,
                                                              unitarySpectrum)
import qualified SDPB.SDP.Types                              as SDP
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB

data SingletScalarBinarySearch = SingletScalarBinarySearch
  { ssbs_bound     :: CFTBound Int SingletScalar
  , ssbs_config    :: BinarySearchConfig Rational
  , ssbs_gapSector :: Int
  } deriving (Show, Generic, Binary, ToJSON)

remoteSingletScalarBinarySearch :: SingletScalarBinarySearch -> Cluster (Bracket Rational)
remoteSingletScalarBinarySearch ssbs = remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ssbs_bound ssbs) `cAp` cPure (ssbs_gapSector ssbs)
  , bsConfig            = ssbs_config ssbs
  , bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound cftBound gapSector gap =
      cftBound { bound = (bound cftBound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (bound cftBound))
    getBool cftBound files result = do
      case SDPB.isDualFeasible result of
        False -> pure ()
        True  -> saveVectors cftBound files result
      pure $ not (SDPB.isDualFeasible result)

-- Differs from SingletScalar in that it saves the coefficients in derivative basis
saveVectors :: CFTBound Int SingletScalar -> CFTBoundFiles -> SDPB.Output -> Job ()
saveVectors cftBound' boundFiles result =
  reifyPrecisionWithFetchContext cftBound' (Proxy @Job) (precision (cftBound' :: CFTBound Int SingletScalar))
  $ \(_ :: Proxy p) -> do
  let cftB = (cftBound' :: CFTBound Int SingletScalar) { precision = Proxy @p }
  norm <- getBoundObject @Job @SingletScalar @p @(Vector (Rounded 'TowardZero p)) cftB boundFiles $
    SDP.normalizationVector . SDP.sdpNormalization . toSDP
  alphaVec <- liftIO $ SDPB.readFunctional norm (outDir boundFiles)
  Log.text "Saving functional"
  DB.insert functionalVectors cftBound' (alphaVec, SDPB.dualObjective result)
  where
    functionalVectors :: DB.KeyValMap (CFTBound Int SingletScalar) (Vector a, Scientific)
    functionalVectors = DB.KeyValMap "functional_vectors"

singletScalarDefaultGaps :: Int -> Rational -> SingletScalar
singletScalarDefaultGaps nmax dPhi = SingletScalar
  { spectrum     = unitarySpectrum
  , objective    = SS.Feasibility
  , blockParams  = block3dParamsNmax nmax
  , spins        = spinsNmax nmax
  , deltaPhi     = dPhi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "SingletScalarB3dAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeBound . go)
  [ (0.5181489, 1.4) -- Should be allowed
  , (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    go (dPhi, deltaS) = CFTBound
      { bound = (singletScalarDefaultGaps nmax dPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalarB3d_binary_search_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (dPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = CFTBound
        { bound = singletScalarDefaultGaps nmax dPhi
        , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
        , solverParams = (jumpFindingParams nmax) { precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 0
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 8
          }
        , threshold  = 1e-30
        , terminateTime = Nothing
        }
      }

boundsProgram p = throwM (UnknownProgram p)
