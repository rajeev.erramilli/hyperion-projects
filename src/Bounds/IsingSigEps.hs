{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.IsingSigEps where

import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Matrix.Static                     (Matrix)
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged (..), untag)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (Coordinate (ZZb),
                                                         CrossingMat,
                                                         DerivMultiplier (..),
                                                         Derivative,
                                                         TaylorCoeff (..),
                                                         Taylors, crossOdd,
                                                         zzbTaylors,
                                                         zzbTaylorsAll)
import qualified SDPB.Blocks                            as Blocks
import qualified SDPB.Blocks.ScalarBlocks               as SB
import           SDPB.Blocks.ScalarBlocks.Build         (scalarBlockBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.BoundDirection             (BoundDirection,
                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         OPECoefficientExternal,
                                                         crossingMatrix,
                                                         crossingMatrixExternal,
                                                         derivsVec, mapBlocks,
                                                         opeCoeffExternalSimple,
                                                         opeCoeffGeneric_,
                                                         opeCoeffIdentical_,
                                                         runTagged)
import           SDPB.Bounds.Spectrum                   (DeltaRange (..),
                                                         Spectrum (..),
                                                         listDeltas)
import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Build.Fetches                     (FetchConfig (..))
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import           SDPB.Math.Linear.Literal               (toV)
import           SDPB.Math.VectorSpace                  (bilinearPair, zero,
                                                         (*^))
import qualified SDPB.Math.VectorSpace                  as VS
import qualified SDPB.SDP.Types                         as SDP

data ExternalDims = ExternalDims
  { deltaSigma :: Rational
  , deltaEps   :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data ExternalOp s = Sig | Eps
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (SB.ScalarRep 3) where
  rep x@Sig = SB.ScalarRep $ deltaSigma (reflect x)
  rep x@Eps = SB.ScalarRep $ deltaEps (reflect x)

data Z2Rep = Z2Even | Z2Odd
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, Binary, ToJSON, FromJSON)

data IsingSigEps = IsingSigEps
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum (Int, Z2Rep)
  , objective    :: Objective
  , spins        :: [Int]
  , blockParams  :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | EpsilonOPEBound (V 2 Rational) BoundDirection
  | StressTensorOPEBound (Maybe (V 2 Rational)) BoundDirection
  | GFFNavigator (Maybe (V 2 Rational))
  | ShadowNavigator (Maybe (V 2 Rational))
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

crossingEquations :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 4 (Taylors 'ZZb, FreeVect b a)
crossingEquations g0 = toV
  ( (zzbTaylors crossOdd, gS Sig Sig Sig Sig)
  , (zzbTaylors crossOdd, gS Eps Eps Eps Eps)
  , (zzbTaylors crossOdd, gS Sig Eps Sig Eps)
  , (zzbTaylorsAll,   gS Sig Sig Eps Eps - gT Eps Sig Sig Eps)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)
    gT a b c d = g0 a b c d (SB.Standard4PtStruct, TChannel)

isingDerivsVec :: IsingSigEps -> V 4 (Vector (TaylorCoeff (Derivative 'ZZb)))
isingDerivsVec i = fmap ($ nmax) (derivsVec crossingEquations)
  where
    nmax = (SB.nmax :: SB.ScalarBlockParams -> Int) (blockParams i)

isingSigEpsCrossingMat
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3) a)
  -> Tagged s (CrossingMat j 4 (SB.ScalarBlock 3) a)
isingSigEpsCrossingMat channel =
  pure $ mapBlocks SB.ScalarBlock $
  crossingMatrix channel (crossingEquations @s)

data Channel j where
  Z2_Even :: SB.SymTensorRep 3 -> Channel 2
  Z2_Odd  :: SB.SymTensorRep 3 -> Channel 1
  IdentityChannel     :: Channel 1
  StressTensorChannel :: Channel 1
  ExternalOpChannel   :: Channel 2

mat
  :: forall s a j . (Reifies s ExternalDims, Fractional a, Eq a)
  => Channel j
  -> Tagged s (CrossingMat j 4 (SB.ScalarBlock 3) a)
mat (Z2_Even internalRep) = isingSigEpsCrossingMat $ toV
  ( opeCoeffIdentical_ Sig internalRep (vec ())
  , opeCoeffIdentical_ Eps internalRep (vec ())
  )

mat (Z2_Odd internalRep) = isingSigEpsCrossingMat $ toV $
  opeCoeffGeneric_ Sig Eps internalRep
    (vec ())
    ((-1)^l *^ vec ())
  where
    l = SB.symTensorSpin internalRep

mat IdentityChannel = isingSigEpsCrossingMat (toV identityOpe)
  where
    identityRep = SB.SymTensorRep (Blocks.Fixed 0) 0
    identityOpe o1 o2
      | o1 == o2  = vec (SB.Standard3PtStruct (rep o1) (rep o2) identityRep)
      | otherwise = 0

mat StressTensorChannel = isingSigEpsCrossingMat (toV stressTensorOpe)
  where
    stressTensorRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 2
    stressTensorOpe o1 o2
      | o1 == o2  =
        fromRational (SB.scalarDelta (rep o1)) *^
        vec (SB.Standard3PtStruct (rep o1) (rep o2) stressTensorRep)
      | otherwise = 0

mat ExternalOpChannel = pure . mapBlocks SB.ScalarBlock $
  crossingMatrixExternal opeCoefficients crossingEquations [Sig, Eps]
  where
    opeCoefficients :: V 2 (OPECoefficientExternal (ExternalOp s) (SB.Standard3PtStruct 3) a)
    opeCoefficients = toV ( opeCoeffExternalSimple Sig Sig Eps (vec ())
                          , opeCoeffExternalSimple Eps Eps Eps (vec ())
                          )

getExternalMat
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => IsingSigEps
  -> f (Matrix 2 2 (Vector a))
getExternalMat i =
  BSDP.getIsolatedMat (blockParams i) (isingDerivsVec i) (runTagged (externalDims i) (mat ExternalOpChannel))

bulkConstraints
  :: forall a s m.
     ( RealFloat a, Binary a, Typeable a
     , Reifies s ExternalDims
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     , Applicative m
     )
  => IsingSigEps
  -> Tagged s [SDP.PositiveConstraint m a]
bulkConstraints i@IsingSigEps{..} = pure $ do
  z2Rep <- [minBound .. maxBound]
  l <- case z2Rep of
    Z2Odd  -> spins
    Z2Even -> filter even spins
  (delta, range) <- listDeltas (l, z2Rep) spectrum
  let intRep = SB.SymTensorRep delta l
  pure $ case z2Rep of
    Z2Even -> BSDP.bootstrapConstraint blockParams dv range $ untag @s $ mat $ Z2_Even intRep
    Z2Odd  -> BSDP.bootstrapConstraint blockParams dv range $ untag @s $ mat $ Z2_Odd  intRep
  where
    dv = isingDerivsVec i

isingSigEpsSDP
  :: forall a m. ( RealFloat a, Applicative m, Binary a, Typeable a
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => IsingSigEps
  -> SDP.SDP m a
isingSigEpsSDP i@IsingSigEps{..} = runTagged externalDims $ do
    epsMat <- mat ExternalOpChannel
    bulk   <- bulkConstraints i
    unit   <- mat IdentityChannel
    stress <- mat StressTensorChannel
    let
      dv = isingDerivsVec i
      epsCons mLambda = case mLambda of
        Nothing     -> BSDP.bootstrapConstraint blockParams dv Isolated epsMat
        Just lambda -> BSDP.bootstrapConstraint blockParams dv Isolated $
                       bilinearPair (fmap fromRational lambda) epsMat
      stressCons = BSDP.bootstrapConstraint blockParams dv Isolated stress
    (cons, obj, norm) <- case objective of
      Feasibility mLambda -> pure
        ( bulk ++ [epsCons mLambda, stressCons]
        , zero
        , unit
        )
      StressTensorOPEBound mLambda dir -> pure
        ( bulk ++ [epsCons mLambda]
        , unit
        , boundDirSign dir *^ stress
        )
      EpsilonOPEBound lambda dir -> pure
        ( bulk ++ [stressCons]
        , unit
        , boundDirSign dir *^ bilinearPair (fmap fromRational lambda) epsMat
        )
      GFFNavigator mLambda -> do
        let ExternalDims { deltaSigma, deltaEps } = externalDims
        sigSq  <- bilinearPair (toV (sqrt 2, 0)) <$>
                  mat (Z2_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaSigma)) 0))
        epsSq  <- bilinearPair (toV (0, sqrt 2)) <$>
                  mat (Z2_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaEps)) 0))
        sigEps <- bilinearPair (toV 1) <$>
                  mat (Z2_Odd  (SB.SymTensorRep (Blocks.Fixed (deltaEps+deltaSigma)) 0))
        let gff = VS.sum [sigSq, epsSq, sigEps]
        pure
          ( bulk ++ [epsCons mLambda, stressCons]
          , unit
          , (-1) *^ gff
          )
      ShadowNavigator mLambda -> do
        let ExternalDims { deltaSigma, deltaEps } = externalDims
        shadow <- bilinearPair (toV 1) <$>
                  mat (Z2_Odd (SB.SymTensorRep (Blocks.Fixed (3-deltaSigma)) 0))
        pure
          ( bulk ++ [epsCons mLambda, stressCons]
          , unit
          , (-1) *^ shadow
          )
    return $ SDP.SDP
      { SDP.sdpObjective     = BSDP.bootstrapObjective blockParams dv obj
      , SDP.sdpNormalization = BSDP.bootstrapNormalization blockParams dv norm
      , SDP.sdpMatrices      = cons
      }

instance ToSDP IsingSigEps where
  type SDPFetchKeys IsingSigEps = '[ SB.BlockTableKey ]
  toSDP = isingSigEpsSDP

instance SDPFetchBuildConfig IsingSigEps where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . SB.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig cftBoundFiles False

-- TODO: Template Haskell?
instance Static (Binary IsingSigEps)              where closureDict = cPtr (static Dict)
instance Static (Show IsingSigEps)                where closureDict = cPtr (static Dict)
instance Static (ToSDP IsingSigEps)               where closureDict = cPtr (static Dict)
instance Static (ToJSON IsingSigEps)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig IsingSigEps) where closureDict = cPtr (static Dict)
instance Static (BuildInJob IsingSigEps)          where closureDict = cPtr (static Dict)
