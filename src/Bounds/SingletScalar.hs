{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.SingletScalar where

import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Functor.Compose                   (Compose (..))
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged (..), untag)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (Coordinate (ZZb),
                                                         CrossingMat,
                                                         DerivMultiplier (..),
                                                         Derivative,
                                                         TaylorCoeff (..),
                                                         Taylors, crossOdd,
                                                         zzbTaylors)
import qualified SDPB.Blocks                            as Blocks
import qualified SDPB.Blocks.ScalarBlocks               as SB
import           SDPB.Blocks.ScalarBlocks.Build         (scalarBlockBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         crossingMatrix,
                                                         derivsVec, mapBlocks,
                                                         opeCoeffIdentical_,
                                                         runTagged)
import           SDPB.Bounds.Spectrum                   (Spectrum (..),
                                                         deltaGap, listDeltas)
import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Build.Fetches                     (FetchConfig (..))
import qualified SDPB.Math.DampedRational               as DR
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import           SDPB.Math.Linear.Literal               (fromM, toV)
import           SDPB.Math.VectorSpace                  (zero)
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = Phi
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s Rational) => HasRep (ExternalOp s) (SB.ScalarRep 3) where
  rep x@Phi = SB.ScalarRep $ reflect x

data SingletScalar = SingletScalar
  { deltaPhi    :: Rational
  , spectrum    :: Spectrum Int
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | TwistGap Int
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

crossingEquations :: forall s a b .
     FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquations g0 = toV
  (zzbTaylors crossOdd, g0 Phi Phi Phi Phi (SB.Standard4PtStruct, SChannel))

singletScalarDerivsVec :: SingletScalar -> V 1 (Vector (TaylorCoeff (Derivative 'ZZb)))
singletScalarDerivsVec i = fmap ($ nmax) (derivsVec crossingEquations)
  where
    nmax = (SB.nmax :: SB.ScalarBlockParams -> Int) (blockParams i)

singletScalarCrossingMat
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3) a)
  -> Tagged s (CrossingMat j 1 (SB.ScalarBlock 3) a)
singletScalarCrossingMat channel =
  pure $ mapBlocks SB.ScalarBlock $
  crossingMatrix channel (crossingEquations @s)

internalMat
  :: forall s a . (Reifies s Rational, Fractional a, Eq a)
  => SB.SymTensorRep 3
  -> Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
internalMat internalRep = singletScalarCrossingMat $ toV $
  opeCoeffIdentical_ (Phi @s) internalRep (vec ())

identityMat
  :: forall a s . (Fractional a, Eq a, Reifies s Rational)
  => Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
identityMat = singletScalarCrossingMat (toV identityOpe)
  where
    identityRep = SB.SymTensorRep (Blocks.Fixed 0) 0
    identityOpe (o1 :: ExternalOp s) o2
      | o1 == o2  = vec (SB.Standard3PtStruct (rep o1) (rep o2) identityRep)
      | otherwise = zero

getInternalBlockVec
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => SB.SymTensorRep 3
  -> SingletScalar
  -> f (DR.DampedRational (Blocks.BlockBase (SB.ScalarBlock 3) a) Vector a)
getInternalBlockVec intRep i =
  fmap (DR.mapNumerator (fromM . getCompose)) $
  BSDP.getContinuumMat (blockParams i) (singletScalarDerivsVec i) $
  runTagged (deltaPhi i) $
  internalMat intRep

bulkConstraints
  :: forall a s m.
     ( RealFloat a, Binary a, Typeable a
     , Reifies s Rational
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     , Applicative m
     )
  => SingletScalar
  -> Tagged s [SDP.PositiveConstraint m a]
bulkConstraints i@SingletScalar{..} = pure $ do
  l <- filter even spins
  (delta, range) <- listDeltas l spectrum
  let intRep = SB.SymTensorRep delta l
  pure $ BSDP.bootstrapConstraint blockParams dv range $ untag @s (internalMat intRep)
  where
    dv = singletScalarDerivsVec i

singletScalarSDP
  :: forall a m. ( RealFloat a, Applicative m, Binary a, Typeable a
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => SingletScalar
  -> SDP.SDP m a
singletScalarSDP i@SingletScalar{..} = runTagged deltaPhi $ do
    bulk   <- bulkConstraints i
    unit   <- identityMat
    let dv = singletScalarDerivsVec i
        (cons, obj, norm) = case objective of
          Feasibility ->
            ( bulk
            , zero `asTypeOf` unit
            , BSDP.bootstrapNormalization blockParams dv unit
            )
          TwistGap l ->
            let
              gapRep :: SB.SymTensorRep 3 = SB.SymTensorRep (deltaGap spectrum l) l
              slopeVec = fmap (DR.evalDerivative 0) (getInternalBlockVec gapRep i)
              -- | Set the slope of the functional at gapRep to 1
              slopeNorm = SDP.Normalization ("slope_" <> BSDP.tshowHash gapRep) slopeVec
            in
              ( bulk
              , unit
              , slopeNorm
              )
    return $ SDP.SDP
      { SDP.sdpObjective     = BSDP.bootstrapObjective blockParams dv obj
      , SDP.sdpNormalization = norm
      , SDP.sdpMatrices      = cons
      }

instance ToSDP SingletScalar where
  type SDPFetchKeys SingletScalar = '[ SB.BlockTableKey ]
  toSDP = singletScalarSDP

instance SDPFetchBuildConfig SingletScalar where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . SB.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig cftBoundFiles False

instance Static (Binary SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (Show SingletScalar)                where closureDict = cPtr (static Dict)
instance Static (ToSDP SingletScalar)               where closureDict = cPtr (static Dict)
instance Static (ToJSON SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig SingletScalar) where closureDict = cPtr (static Dict)
instance Static (BuildInJob SingletScalar)          where closureDict = cPtr (static Dict)
