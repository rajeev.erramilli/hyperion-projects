{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.GNYSigPsiEps where

import           Bounds.GNY                             (ChannelType (..),
                                                         ON3PtStruct (..),
                                                         ON4PtStruct (..),
                                                         ONRep (..),
                                                         evalONBlock, so3)
import qualified Bounds.GNY                             as GNY
import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Matrix.Static                     (Matrix)
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged)
import           Data.Traversable                       (for)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (BlockFetchContext,
                                                         Coordinate (XT),
                                                         CrossingMat,
                                                         Delta (..),
                                                         Derivative (..),
                                                         TaylorCoeff (..),
                                                         Taylors, unzipBlock,
                                                         xEven, xOdd, xtTaylors,
                                                         yEven, yOdd)
import qualified SDPB.Blocks.Blocks3d                   as B3d
import           SDPB.Blocks.Blocks3d.Build             (block3dBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.BoundDirection             (BoundDirection,
                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         OPECoefficientExternal,
                                                         ThreePointStructure (..),
                                                         crossingMatrix,
                                                         crossingMatrixExternal,
                                                         derivsVec, map4pt,
                                                         mapBlocksFreeVect,
                                                         mapOps,
                                                         opeCoeffGeneric_,
                                                         opeCoeffIdentical_,
                                                         runTagged2)
import           SDPB.Bounds.Spectrum                   (DeltaRange, Spectrum,
                                                         listDeltas)
import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Build.Fetches                     (FetchConfig (..))
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import qualified SDPB.Math.FreeVect                     as FV
import           SDPB.Math.HalfInteger                  (HalfInteger)
import qualified SDPB.Math.HalfInteger                  as HI
import           SDPB.Math.Linear.Literal               (toV)
import qualified SDPB.Math.Linear.Util                  as L
import           SDPB.Math.VectorSpace                  (bilinearPair, zero,
                                                         (*^))
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = Sig | Psi | Eps
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaSig :: Rational
  , deltaPsi :: Rational
  , deltaEps :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (B3d.ConformalRep Rational, ONRep) where
  rep x = case x of
    Sig -> (B3d.ConformalRep dSig 0,     ONSinglet)
    Psi -> (B3d.ConformalRep dPsi (1/2), ONVector)
    Eps -> (B3d.ConformalRep dEps 0,     ONSinglet)
    where
      ExternalDims { deltaSig = dSig, deltaPsi = dPsi, deltaEps = dEps} = reflect x

data GNYSigPsiEps = GNYSigPsiEps
  { externalDims :: ExternalDims
  , nGroup       :: Rational
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 4 Rational))
  | StressTensorOPEBound BoundDirection
  -- | ExternalOPEBound BoundDirection
  -- | ConservedCurrentOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- | A channel with crossing matrices of size jxj containing blocks of
-- type 'b'
data Channel j b where
  Scalar_ParityEven_Singlet        :: Delta                  -> Channel 3 B3d.Block3d
  SpinEven_ParityEven_Singlet      :: B3d.ConformalRep Delta -> Channel 4 B3d.Block3d
  SpinEven_ParityOdd_Singlet       :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinOdd_ParityOdd_Singlet        :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  Scalar_ParityEven_TensorSym      :: Delta                  -> Channel 1 B3d.Block3d
  SpinEven_ParityEven_TensorSym    :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinEven_ParityOdd_TensorSym     :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityOdd_TensorSym      :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityEven_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinOdd_ParityOdd_TensorAntiSym  :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinEven_ParityOdd_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityEven_Vector                :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  ParityOdd_Vector                 :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  IdentityChannel     :: Channel 1 B3d.IdentityBlock3d
  StressTensorChannel :: Channel 1 B3d.Block3d
  CurrentChannel      :: Channel 2 B3d.Block3d
  ExternalOpChannel   :: Channel 4 B3d.Block3d

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: KnownNat j -- ^ Existentially quantify over j
    => DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel j B3d.Block3d -- ^ The associated Channel
    -> BulkConstraint


------------------ Crossing equations ---------------------


-- | Crossing equations for <psi psi sig eps >, following Aike's notes

crossingEqsFFES
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 6 (Taylors 'XT, FreeVect b a)
crossingEqsFFES g0 = toV
  (( xtTaylors  xOdd  yEven, g s e u d yEven - g u e s d yEven )     -- ^ line 1
  , ( xtTaylors xEven yOdd,  g s e u d yOdd - g u e s d yOdd  )     -- ^ line 2
  , ( xtTaylors xEven yEven, g s e u d yEven + g u e s d yEven )     -- ^ line 4
  , ( xtTaylors xOdd  yOdd,  g s e u d yOdd  + g u e s d yOdd  )     -- ^ line 5
  ,  ( xtTaylors xOdd yEven,  g u s d e yEven  )                       -- ^ line 3
  , ( xtTaylors xOdd yOdd, g u s d e yOdd )                       -- ^ line 6
  )
  where
    g (o1,q1) (o2,q2) (o3,q3) (o4,q4) ySign =
      g0 o1 o2 o3 o4 (B3d.Q4Struct (q1,q2,q3,q4) ySign)
    u = (Psi, 1/2)
    d = (Psi, -1/2)
    s = (Sig, 0)
    e = (Eps, 0)



-- | Crossing equations for <sig sig eps eps>
crossingEqSSEE
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 3 (Taylors 'XT, FreeVect b a)
crossingEqSSEE g0 = toV
  ( ( xtTaylors xOdd yEven, g s e s e )
  , ( xtTaylors xOdd yEven, g s s e e + g e s s e)
  , ( xtTaylors xEven yEven, g s s e e - g e s s e)
  )
  where
    g o1 o2 o3 o4 = g0 o1 o2 o3 o4 (B3d.Q4Struct (0,0,0,0) yEven )
    e = Eps
    s = Sig

--crossingEqEEEE
--  :: forall s a b .
--     FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
--  -> V 1 (Taylors 'XT, FreeVect b a)
--crossingEqEEEE g = toV
--  (xtTaylors xOdd yEven, g Eps Eps Eps Eps (B3d.Q4Struct (0,0,0,0) yEven))


-- | Crossing equations of the GNY model with N /= 1
crossingEqsGNYSigPsiEps
  :: forall n s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (B3d.Q4Struct, ON4PtStruct n) b a
  -> V 38 (Taylors 'XT, FreeVect b a)
crossingEqsGNYSigPsiEps g =
  GNY.crossingEqsGNY (mapOps f1 g)
  L.++ GNY.crossingEqsFFSS (mapOps f2 (map4pt (,Unique) g))
  L.++ GNY.crossingEqSSSS (mapOps f2 (map4pt (,Unique) g))
  L.++ crossingEqsFFES (map4pt (,Unique) g)
  L.++ crossingEqSSEE (map4pt (,Unique) g)
  where
    f1 :: GNY.ExternalOp s -> ExternalOp t
    f1 GNY.Sig = Sig
    f1 GNY.Psi = Psi
    f2 :: GNY.ExternalOp s -> ExternalOp t
    f2 GNY.Sig = Eps
    f2 GNY.Psi = Psi

-- | Crossing equations of the GNY model with N=1 (or the N=1 SUSY
-- Ising model, without SUSY taken into account)
--crossingEqsN1Eps
--  :: forall s a b . (Ord b, Fractional a, Eq a)
--  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
--  -> V 12 (Taylors 'XT, FreeVect b a)
--crossingEqsN1 g =
--  FFFF.crossingEqsWithFlavorSign xEven (mapOps (const Psi) g) L.++
--  crossingEqsFFSS g L.++
--  crossingEqSSSS  g

derivsVecGNYSigPsiEps :: GNYSigPsiEps -> V 38 (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecGNYSigPsiEps f =
  fmap ($ B3d.nmax (blockParams f)) (derivsVec crossingEqsGNYSigPsiEps)

crossingMatGNYSigPsiEps
  :: forall j n s a. (Reifies n Rational, KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
  -> Tagged '(n,s) (CrossingMat j 38 B3d.Block3d a)
crossingMatGNYSigPsiEps channel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix channel ((crossingEqsGNYSigPsiEps @n @s))
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)


mat
  :: forall j b n s a . (Reifies s ExternalDims, Reifies n Rational, Fractional a, Floating a, Eq a)
  => Channel j b
  ->  Tagged '(n,s) (CrossingMat j 38 b a)

-- Singlet r1 r2
mat (Scalar_ParityEven_Singlet deltaScalar) = crossingMatGNYSigPsiEps $ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 0))
  , opeCoeffIdentical_ Sig iRep (so3 0 0)
  , opeCoeffIdentical_ Eps iRep (so3 0 0)
  )
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSinglet)

mat (SpinEven_ParityEven_Singlet iConformalRep) = crossingMatGNYSigPsiEps $ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  , opeCoeffIdentical_ Sig iRep (so3 0 l)
  , opeCoeffIdentical_ Eps iRep (so3 0 l)
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep

-- Singlet r3
mat (SpinEven_ParityOdd_Singlet iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3
  , opeCoeffGeneric_ Sig Eps iRep (so3 0 l) ((-1)^(round l :: Int) *^ (so3 0 l))
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Singlet r4
mat (SpinOdd_ParityOdd_Singlet iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4
  , opeCoeffGeneric_ Sig Eps iRep (so3 0 l) ((-1)^(round l :: Int) *^ (so3 0 l))
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Symmetric Tensor r1 r2
-- (Essentially the same as Singlet)
mat (Scalar_ParityEven_TensorSym deltaScalar) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep  ((so3 0 0))
  )
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSymTensor)

mat (SpinEven_ParityEven_TensorSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep

-- Symmetric Tensor  r3
mat (SpinEven_ParityOdd_TensorSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Symmetric Tensor  r4
mat (SpinOdd_ParityOdd_TensorSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Antisymmetric Tensor r1 r2 (exchange even and odd spin )
mat (SpinOdd_ParityEven_TensorAntiSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep

-- Antisymmetric Tensor r3
mat (SpinOdd_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Antisymmetric Tensor r4
mat (SpinEven_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)



-- psi * sig , psi * eps
-- Flipped signs
mat (ParityEven_Vector iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffGeneric_ Psi Sig iRep (so3 (1/2) (l-1/2)) ((-1)^(round (l+1/2) :: Int) *^ (so3 (1/2) (l+1/2)))
  , opeCoeffGeneric_ Psi Eps iRep (so3 (1/2) (l+1/2)) ((-1)^(round (l-1/2) :: Int) *^ (so3 (1/2) (l-1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep

mat (ParityOdd_Vector iConformalRep) = crossingMatGNYSigPsiEps$ toV
  ( opeCoeffGeneric_ Psi Sig iRep (so3 (1/2) (l+1/2)) ((-1)^(round (l-1/2) :: Int) *^ (so3 (1/2) (l-1/2)))
  , opeCoeffGeneric_ Psi Eps iRep (so3 (1/2) (l-1/2)) ((-1)^(round (l+1/2) :: Int) *^ (so3 (1/2) (l+1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep


-- Identity
mat IdentityChannel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix (toV identityOpe) (crossingEqsGNYSigPsiEps @n @s)
  where
    evalFlavorCoeff (b,f) =  evalONBlock f *^ vec (B3d.IdentityBlock3d b)
    identityOpe o1 o2
      | o1 == o2  = vec ((fst o1rep, ON3PtStruct (snd o1rep) (snd o1rep) ONSinglet))
      | otherwise = 0
      where o1rep = rep @(ExternalOp s) o1


-- StressTensor
mat StressTensorChannel = crossingMatGNYSigPsiEps(toV stressTensorOpe)
  where
    stressTensorRep = (B3d.ConformalRep (RelativeUnitarity 0) 2, ONSinglet)
    psiTStruct = - 3/(2*sqrt 2*pi) *^ so3 1 2 + (sqrt 3*dPsi)/(2*pi) *^ so3 0 2
    sigTStruct = - (sqrt 3*dSig)/(2*sqrt 2*pi) *^ so3 0 2
    epsTStruct = - (sqrt 3*dEps)/(2*sqrt 2*pi) *^ so3 0 2
    dPsi = fromRational (deltaPsi (reflect @s Psi))
    dSig = fromRational (deltaSig (reflect @s Sig))
    dEps = fromRational (deltaEps (reflect @s Eps))
    stressTensorOpe o1 o2 =
      FV.mapBasis (makeStructure (rep o1) (rep o2) stressTensorRep) $
      case (o1, o2) of
        (Psi, Psi) -> psiTStruct
        (Sig, Sig) -> sigTStruct
        (Eps, Eps) -> epsTStruct
        _          -> 0


-- Conserved Current
mat CurrentChannel = mat (SpinOdd_ParityEven_TensorAntiSym (fst currentRep))
  where
    currentRep = (B3d.ConformalRep (RelativeUnitarity 0) 1, ONAntiSymTensor)
    -- Not using the Ward identity for now
    -- psiTStruct = - 1/(sqrt 2*pi) *^ so3 0 1
    -- fixedCurrentOPE o1 o2
    --   | (o1 == o2 && o1 == Psi)  = FV.mapBasis (makeStructure (rep o1) (rep o1) (currentRep)) psiTStruct
    --   | otherwise = zero


mat ExternalOpChannel =
  pure . mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsGNYSigPsiEps @n) [Sig,Psi,Eps]
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)
    opeCoefficients :: V 4 (OPECoefficientExternal (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
    opeCoefficients = toV $ ((\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                             case (o1, o2, o3) of
                             (Psi,Psi,Sig) -> - so3 1 1
                             (Psi,Sig,Psi) -> so3 (1/2) 0
                             (Sig,Psi,Psi) -> - so3 (1/2) 1 -- Flipped sign 
                             _             -> 0)
                            , (\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                             case (o1, o2, o3) of
                             (Psi,Psi,Eps) ->  so3 0 0  -- Flipped sign, the signs of other psi*psi*O channels are not flipped
                             (Psi,Eps,Psi) -> so3 (1/2) 1
                             (Eps,Psi,Psi) -> so3 (1/2) 0 -- Flipped sign
                             _             -> 0)
                            , (\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                             case (o1, o2, o3) of
                             (Sig,Eps,Sig) -> so3 0 0
                             (Sig,Sig,Eps) -> so3 0 0
                             (Eps,Sig,Sig) -> so3 0 0
                             _             -> 0)
                            , (\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                            case (o1, o2, o3) of
                            (Eps,Eps,Eps) -> so3 0 0
                            _             -> 0)
                             )

getExternalMat
  :: (BlockFetchContext B3d.Block3d a f, Applicative f, RealFloat a)
  => GNYSigPsiEps
  -> f (Matrix 4 4 (Vector a))
getExternalMat g =
  BSDP.getIsolatedMat (blockParams g) (derivsVecGNYSigPsiEps g) (runTagged2 (nGroup g,externalDims g) (mat ExternalOpChannel))

bulkConstraints :: GNYSigPsiEps -> [BulkConstraint]
bulkConstraints f = do
  parity <- [minBound .. maxBound]
  oNRep <- [minBound .. maxBound]
  l <- case (parity, oNRep) of
    (_, ONVector)                    -> filter (not . HI.isInteger) (spins f)
    (B3d.ParityEven,ONSinglet)       -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSinglet)       -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONSymTensor)     -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSymTensor)     -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONAntiSymTensor) -> filter HI.isOddInteger (spins f)
    (B3d.ParityOdd, ONAntiSymTensor) -> filter (\x -> (x/=0) && (HI.isInteger x)) (spins f)
  (delta, range) <- listDeltas (ChannelType l parity oNRep) (spectrum f)
  let iConformalRep = B3d.ConformalRep delta l
  pure $ case (parity, HI.isEvenInteger l, l, oNRep) of
    (B3d.ParityEven, True,  0, ONSinglet)       -> BulkConstraint range $ Scalar_ParityEven_Singlet delta
    (B3d.ParityEven, True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityEven_Singlet iConformalRep
    (B3d.ParityOdd,  True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityOdd_Singlet iConformalRep
    (B3d.ParityOdd,  False, _, ONSinglet)       -> BulkConstraint range $ SpinOdd_ParityOdd_Singlet iConformalRep
    (B3d.ParityEven, True,  0, ONSymTensor)     -> BulkConstraint range $ Scalar_ParityEven_TensorSym delta
    (B3d.ParityEven, True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityEven_TensorSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityOdd_TensorSym iConformalRep
    (B3d.ParityOdd,  False, _, ONSymTensor)     -> BulkConstraint range $ SpinOdd_ParityOdd_TensorSym iConformalRep
    (B3d.ParityEven, False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityEven_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONAntiSymTensor) -> BulkConstraint range $ SpinEven_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityEven, False, _, ONVector)        -> BulkConstraint range $ ParityEven_Vector iConformalRep
    (B3d.ParityOdd,  False, _, ONVector)        -> BulkConstraint range $ ParityOdd_Vector iConformalRep
    _ -> error $ "Internal representation disallowed: " ++ show((parity,l,oNRep))

gnyepsSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext B3d.Block3d a m
     )
  => GNYSigPsiEps
  -> SDP.SDP m a
gnyepsSDP f@GNYSigPsiEps{..} = runTagged2 (nGroup,externalDims) $ do
  let dv = derivsVecGNYSigPsiEps f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  extMat <- mat ExternalOpChannel
  unit   <- mat IdentityChannel
  stress <- mat StressTensorChannel
  current <- mat CurrentChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv stress --Isolated stress
    extCons mLambda = case mLambda of
      Nothing -> BSDP.isolatedConstraint blockParams dv extMat
      Just lambda -> BSDP.isolatedConstraint blockParams dv $
                       bilinearPair (fmap fromRational lambda) extMat
    currentCons = BSDP.isolatedConstraint blockParams dv current
    (cons, obj, norm) = case objective of
      Feasibility mLambda ->
        ( bulk ++ [extCons mLambda, stressCons, currentCons]
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
      StressTensorOPEBound dir ->
        ( bulk ++ [extCons Nothing, currentCons]
        , BSDP.bootstrapObjective blockParams dv $ unit
        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
        )
      -- ConservedCurrentOPEBound dir ->
      --   ( bulk ++ [extCons, stressCons]
      --   , BSDP.bootstrapObjective blockParams dv $ unit
      --   , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ current
      --   )
      --ExternalOPEBound dir ->
      --  ( bulk ++ [stressCons, currentCons]
      --  , BSDP.bootstrapObjective blockParams dv $ unit
      --  , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ extFS
      --  )
  return $ SDP.SDP
    { SDP.sdpObjective     = obj
    , SDP.sdpNormalization = norm
    , SDP.sdpMatrices      = cons
    }

instance ToSDP GNYSigPsiEps where
  type SDPFetchKeys GNYSigPsiEps = '[ B3d.BlockTableKey ]
  toSDP = gnyepsSDP

instance SDPFetchBuildConfig GNYSigPsiEps where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . B3d.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig cftBoundFiles

instance Static (Binary GNYSigPsiEps)              where closureDict = cPtr (static Dict)
instance Static (Show GNYSigPsiEps)                where closureDict = cPtr (static Dict)
instance Static (ToSDP GNYSigPsiEps)               where closureDict = cPtr (static Dict)
instance Static (ToJSON GNYSigPsiEps)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig GNYSigPsiEps) where closureDict = cPtr (static Dict)
instance Static (BuildInJob GNYSigPsiEps)          where closureDict = cPtr (static Dict)
