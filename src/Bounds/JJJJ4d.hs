{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UnicodeSyntax         #-}

module Bounds.JJJJ4d where

import           Data.Aeson                                        (FromJSON,
                                                                    ToJSON)
import           Data.Binary                                       (Binary)
import           Data.Data                                         (Typeable)
import           Data.Vector                                       (Vector)
import           GHC.Generics                                      (Generic)
import           GHC.TypeNats                                      (KnownNat)
import           Linear.V                                          (V)
import           SDPB.Blocks                                       (BlockFetchContext,
                                                                    Coordinate (XT),
                                                                    CrossingMat,
                                                                    DerivMultiplier (..),
                                                                    Derivative (..),
                                                                    TaylorCoeff (..),
                                                                    Taylors,
                                                                    xOdd,
                                                                    xtTaylors,
                                                                    xtTaylorsAll,
                                                                    yEven)
import qualified SDPB.Blocks.Blocks4d                              as B4d
import           SDPB.Blocks.Blocks4d.Collection.JJJJ
import           SDPB.Blocks.Blocks4d.Collection.JJJJ.Descriptions (JJJJDescriptionName)
import           SDPB.Blocks.Blocks4d.Util                         (Parity (..),
                                                                    parity)
import qualified SDPB.Bounds.BootstrapSDP                          as BSDP
--import           SDPB.Bounds.BoundDirection             (BoundDirection,
--                                                         boundDirSign)
import           Control.Monad.IO.Class                            (liftIO)
import           Hyperion                                          (Dict (..), Static (..),
                                                                    cPtr)
import           Hyperion.Bootstrap.CFTBound                       (BuildInJob,
                                                                    SDPFetchBuildConfig (..),
                                                                    ToSDP (..),
                                                                    blockDir)
import           SDPB.Bounds.Crossing.CrossingEquations            (FourPointFunctionTerm,
                                                                    OPECoefficient,
                                                                    crossingMatrix,
                                                                    derivsVec,
                                                                    mapBlocks,
                                                                    opeCoeffIdentical)
import           SDPB.Bounds.Spectrum                              (Spectrum,
                                                                    listDeltas)
import           SDPB.Build.BuildLink                              (SomeBuildChain (..))
import           SDPB.Build.Fetches                                (FetchConfig (..))
import           SDPB.Math.FreeVect                                (FreeVect,
                                                                    vec)
import           SDPB.Math.Linear.Literal                          (toV)
import           SDPB.Math.VectorSpace                             (zero)
import qualified SDPB.SDP.Types                                    as SDP

data ExternalOp = J
  deriving (Show, Eq, Ord, Enum, Bounded)

--instance (Reifies s Rational) => HasRep (ExternalOp s) (B3d.ConformalRep Rational) where
--  rep x = B3d.ConformalRep (reflect x) (1/2)

data JJJJ4d = JJJJ4d
  { spectrum    :: Spectrum (Int, Int)
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: B4d.Block4dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
--  | StressTensorOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

class ThreePointStructure s i r ri where
  makeStructure :: r -> r -> ri -> i -> s

crossingEquations :: forall a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp) JJJJFourPointFunctional b a
  -> V 5 (Taylors 'XT, FreeVect b a)
crossingEquations g0 = toV
  ( (xtTaylors xOdd yEven,  gS 1)
  , (xtTaylorsAll   yEven,  gS 2 - gT 4)
  , (xtTaylors xOdd yEven,  gS 3)
  , (xtTaylorsAll   yEven,  gS 5 - gT 7)
  , (xtTaylors xOdd yEven,  gS 6)
  )
  where
    gS i = g0 J J J J (JJJJFourPointFunctional i SChannel)
    gT i = g0 J J J J (JJJJFourPointFunctional i TChannel)

jjjj4dDerivsVec :: JJJJ4d -> V 5 (Vector (TaylorCoeff (Derivative 'XT)))
jjjj4dDerivsVec f =
  fmap ($ B4d.nmax (blockParams f)) (derivsVec crossingEquations)

jjjj4dCrossingMat
  :: forall j a. (KnownNat j, Fractional a, Eq a)
  => [Rational]
  -> V j (OPECoefficient (ExternalOp) JJJJThreePointStructure a)
  -> CrossingMat j 5 (B4d.Block4d JJJJBlock) a
jjjj4dCrossingMat poles channel =
  mapBlocks (B4d.Block4d poles) $
  crossingMatrix channel crossingEquations

p0SpinEvenMat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 2 5 (B4d.Block4d JJJJBlock) a
p0SpinEvenMat internalRep@(JJJJInternalRep l 0 _) | even l = jjjj4dCrossingMat [] $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1, 2)
p0SpinEvenMat _ = error "Absurd irrep in call to p0SpinEvenMat"

p0SpinOddMat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 1 5 (B4d.Block4d JJJJBlock) a
p0SpinOddMat internalRep@(JJJJInternalRep l 0 _) | odd l = jjjj4dCrossingMat [] $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1)
p0SpinOddMat _ = error "Absurd irrep in call to p0SpinOddMat"

p0Spin0Mat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 1 5 (B4d.Block4d JJJJBlock) a
p0Spin0Mat internalRep@(JJJJInternalRep 0 0 _) = jjjj4dCrossingMat [] $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1)
p0Spin0Mat _ = error "Absurd irrep in call to p0Spin0Mat"

p2SpinEvenMat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 1 5 (B4d.Block4d JJJJBlock) a
p2SpinEvenMat internalRep@(JJJJInternalRep l 2 _) | even l = jjjj4dCrossingMat [] $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1)
p2SpinEvenMat _ = error "Absurd irrep in call to p2SpinEvenMat"

p2SpinOddMat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 1 5 (B4d.Block4d JJJJBlock) a
p2SpinOddMat internalRep@(JJJJInternalRep l 2 _) | odd l = jjjj4dCrossingMat poles $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1)
  where
    poles = if l == 1 then [4] else []
p2SpinOddMat _ = error "Absurd irrep in call to p2SpinOddMat"

p4SpinEvenMat
  :: forall a . (Fractional a, Eq a)
  => JJJJInternalRep
  -> CrossingMat 1 5 (B4d.Block4d JJJJBlock) a
p4SpinEvenMat internalRep@(JJJJInternalRep l 4 _) | even l = jjjj4dCrossingMat poles $
  fmap (opeCoeffIdentical J . vec . JJJJThreePointStructure internalRep) $ toV (1)
  where poles = if l == 0 then [] else [fromIntegral l+4]
p4SpinEvenMat _ = error "Absurd irrep in call to p4SpinEvenMat"


identityMat
  :: forall a . (Fractional a, Eq a)
  => CrossingMat 1 5 JJJJIdentityBlock a
identityMat =
  mapBlocks JJJJIdentityBlock $
  crossingMatrix (toV identityOpe) crossingEquations
  where
    identityOpe o1 o2
      | o1 == o2  = vec ()
      | otherwise = zero

--stressTensorMat
--  :: forall s a . (Floating a, Eq a, Reifies s Rational)
--  => Tagged s (CrossingMat 1 5 B3d.Block3d a)
--stressTensorMat = ffCrossingMat $
--  fmap (opeCoeffIdentical_ (Psi @s) stressTensorRep) $ toV stressTensorStruct
--  where
--    stressTensorStruct =
--      3/(2*sqrt 2*pi) *^ vec (B3d.SO3StructLabel 1 2)
--      - (sqrt 3*dPsi)/(2*pi) *^ vec (B3d.SO3StructLabel 0 2)
--    stressTensorRep = B3d.ConformalRep (RelativeUnitarity 0) 2
--    dPsi = fromRational (reflect @s Psi)

bulkConstraints
  :: forall a m.
     ( RealFloat a, Binary a, Typeable a
     , BlockFetchContext (B4d.Block4d JJJJBlock) a m
     , Applicative m
     )
  => JJJJ4d
  -> [SDP.PositiveConstraint m a]
bulkConstraints j@JJJJ4d{..} = do
  p <- [0, 2, 4]
  l <- case p of
    0 -> spins
    2 -> filter (>0) spins
    4 -> filter even spins
    _ -> error "Absurd"
  (delta, range) <- listDeltas (l, p) spectrum
  let internalRep = JJJJInternalRep l p delta
  pure $ case (p, parity l, l) of
    (0, Even, 0) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p0Spin0Mat internalRep
    (0, Even, _) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p0SpinEvenMat internalRep
    (0, Odd, _) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p0SpinOddMat internalRep
    (2, Even, _) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p2SpinEvenMat internalRep
    (2, Odd, _) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p2SpinOddMat internalRep
    (4, Even, _) ->
      BSDP.bootstrapConstraint @(B4d.Block4d JJJJBlock) blockParams dv range $
      p4SpinEvenMat internalRep
    _ -> error "Internal representation disallowed"
  where
    dv = jjjj4dDerivsVec j

jjjj4dSDP
  :: forall a m. ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext (B4d.Block4d (JJJJBlock)) a m
     )
  => JJJJ4d
  -> SDP.SDP m a
jjjj4dSDP j@JJJJ4d{..} =
  let
    bulk = bulkConstraints j
    unit = identityMat
--    stress <- stressTensorMat
--    stressConstraint = BSDP.isolatedConstraint blockParams dv stress
    dv = jjjj4dDerivsVec j
    (cons, obj, norm) = case objective of
      Feasibility ->
        ( -- stressConstraint :
          bulk
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
--      StressTensorOPEBound dir ->
--        ( bulk
--        , BSDP.bootstrapObjective blockParams dv unit
--        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
--        )
  in
  SDP.SDP
    { SDP.sdpObjective     = obj
    , SDP.sdpNormalization = norm
    , SDP.sdpMatrices      = cons
    }

instance SDPFetchBuildConfig JJJJ4d where
  sdpFetchConfig _ _ files =
    liftIO . B4d.readBlockTable (blockDir files) :&: FetchNil
  sdpDepBuildChain _ config files = SomeBuildChain $ B4d.block4dBuildChain config files

instance ToSDP JJJJ4d where
  type SDPFetchKeys JJJJ4d = '[ B4d.BlockTableKey JJJJDescriptionName Int ]
  toSDP = jjjj4dSDP

instance Static (Binary JJJJ4d)              where closureDict = cPtr (static Dict)
instance Static (Show JJJJ4d)                where closureDict = cPtr (static Dict)
instance Static (ToSDP JJJJ4d)               where closureDict = cPtr (static Dict)
instance Static (ToJSON JJJJ4d)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig JJJJ4d) where closureDict = cPtr (static Dict)
instance Static (BuildInJob JJJJ4d)          where closureDict = cPtr (static Dict)
