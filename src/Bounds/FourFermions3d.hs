{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.FourFermions3d where

import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged)
import           Data.Traversable                       (for)
import           Data.Vector                            (Vector)
import qualified Data.Vector                            as V
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (BlockFetchContext,
                                                         Coordinate (XT),
                                                         CoordinateDir (XDir),
                                                         CrossingMat,
                                                         Delta (..),
                                                         Derivative (..),
                                                         Sign (..),
                                                         TaylorCoeff (..),
                                                         Taylors, xEven, xOdd,
                                                         xtTaylors, yEven, yOdd)
import qualified SDPB.Blocks.Blocks3d                   as B3d
import           SDPB.Blocks.Blocks3d.Build             (block3dBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.BoundDirection             (BoundDirection,
                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         crossingMatrix,
                                                         derivsVec, mapBlocks,
                                                         opeCoeffIdentical_,
                                                         runTagged)
import           SDPB.Bounds.Spectrum                   (DeltaRange, Spectrum,
                                                         listDeltas)
import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Build.Fetches                     (FetchConfig (..))
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import           SDPB.Math.HalfInteger                  (HalfInteger)
import           SDPB.Math.Linear.Literal               (toV)
import           SDPB.Math.VectorSpace                  (zero, (*^))
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = Psi
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s Rational) => HasRep (ExternalOp s) (B3d.ConformalRep Rational) where
  rep psi = B3d.ConformalRep (reflect psi) (1/2)

data FourFermions3d = FourFermions3d
  { deltaPsi    :: Rational
  , spectrum    :: Spectrum (Int, B3d.Parity)
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | StressTensorOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- | A channel with crossing matrices of size jxj containing blocks of
-- type 'b'
data Channel j b where
  ParityEven_SpinEven :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  -- | When l=0, only one of the parity even spin even structures is
  -- allowed
  ParityEven_Scalar   :: Delta                  -> Channel 1 B3d.Block3d
  ParityOdd_SpinEven  :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityOdd_SpinOdd   :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  IdentityChannel     :: Channel 1 B3d.IdentityBlock3d
  StressTensorChannel :: Channel 1 B3d.Block3d

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: KnownNat j -- ^ Existentially quantify over j
    => DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel j B3d.Block3d -- ^ The associated Channel
    -> BulkConstraint

-- | Equations A.10 in https://arxiv.org/pdf/1612.08987.pdf [1]
--
-- Note that the conventions for x,t coordinates in [1] are different
-- from what blocks_3d uses. Firstly, blocks_3d defines t = ((z -
-- zb)/2)^2, whereas [1] uses (z-zb)/2. Furthermore, as described in
-- https://gitlab.com/bootstrapcollaboration/blocks_3d/-/blob/master/doc/conventions.pdf,
-- blocks_3d supplies an extra factor of p_-(x,t)=2/(z-zb) when the
-- 4pt sign is Minus. Thus, the conversion is as follows:
--
-- > \partial_t^{2m}   from [1] -> \partial_t^m when 4pt sign is Plus
-- > \partial_t^{2m+1} from [1] -> \partial_t^m when 4pt sign is Minus
--
-- We include an optional flavor sign 'fSign' for the x-derivatives,
-- since this is needed in the GNY model.
crossingEqsWithFlavorSign
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => Sign 'XDir
  -> FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 5 (Taylors 'XT, FreeVect b a)
crossingEqsWithFlavorSign fSign g0 = toV
  ( (V.filter positiveN .
     xtTaylors (fSign<>xOdd)  yEven, g (u,u,u,u) yEven)
  , (xtTaylors (fSign<>xOdd)  yEven, g (u,d,u,d) yEven)
  , (xtTaylors (fSign<>xOdd)  yEven, g (u,u,d,d) yEven + g (d,u,u,d) yEven)
  , (xtTaylors (fSign<>xEven) yEven, g (u,u,d,d) yEven - g (d,u,u,d) yEven)
  , (xtTaylors (fSign<>xEven) yOdd,  g (u,u,u,u) yOdd)
  )
  where
    u = 1/2
    d = -1/2
    g qs sgn = g0 Psi Psi Psi Psi (B3d.Q4Struct qs sgn)
    positiveN (TaylorCoeff (Derivative (_,n))) = n > 0

crossingEqs
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 5 (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsWithFlavorSign xEven

derivsVecFF :: FourFermions3d -> V 5 (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecFF f =
  fmap ($ B3d.nmax (blockParams f)) (derivsVec crossingEqs)

crossingMatFF
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (B3d.SO3Struct Rational Rational Delta) a)
  -> Tagged s (CrossingMat j 5 B3d.Block3d a)
crossingMatFF channel =
  pure $ mapBlocks B3d.Block3d $
  crossingMatrix channel (crossingEqs @s)

so3 :: (Num a, Eq a) => HalfInteger -> HalfInteger -> FreeVect B3d.SO3StructLabel a
so3 j12 j123 = vec (B3d.SO3StructLabel j12 j123)

mat
  :: forall s a j b . (Reifies s Rational, Floating a, Eq a)
  => Channel j b
  -> Tagged s (CrossingMat j 5 b a)
mat (ParityEven_SpinEven internalRep) = crossingMatFF $
  fmap (opeCoeffIdentical_ Psi internalRep) $
  -- | These structures are related by an l-dependent linear
  -- transformation to r1, r2 from [1]
  toV (so3 0 l, so3 1 l)
  where
    l = B3d.spin internalRep

mat (ParityEven_Scalar deltaScalar) = crossingMatFF $
  fmap (opeCoeffIdentical_ Psi internalRep) $ toV $ so3 0 0
  where
    internalRep = B3d.ConformalRep deltaScalar 0

mat (ParityOdd_SpinEven internalRep) = crossingMatFF $
  fmap (opeCoeffIdentical_ Psi internalRep) $ toV r3
  where
    l = B3d.spin internalRep
    -- | This is related by an l-dependent coefficient to r3 from [1]
    -- Note that the first term vanishes if l=0. Here, we are relying
    -- on 'sqrt (realToFrac 0) == 0', which I sure hope is true!
    r3 = - sqrt (realToFrac l)     *^ so3 1 (l-1)
         + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

mat (ParityOdd_SpinOdd internalRep) = crossingMatFF $
  fmap (opeCoeffIdentical_ Psi internalRep) $ toV r4
  where
    l = B3d.spin internalRep
    -- | This is related by an l-dependent coefficient to r4 from [1]
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1)
         + sqrt (realToFrac l)   *^ so3 1 (l+1)

mat IdentityChannel =
  pure $ mapBlocks B3d.IdentityBlock3d $
  crossingMatrix (toV identityOpe) (crossingEqs @s)
  where
    identityOpe o1 o2
      | o1 == o2  = vec (rep @(ExternalOp s) o1)
      | otherwise = 0

mat StressTensorChannel = crossingMatFF $
  fmap (opeCoeffIdentical_ Psi stressTensorRep) $ toV stressTensorStruct
  where
    stressTensorStruct =
      3/(2*sqrt 2*pi) *^ so3 1 2 - (sqrt 3*dPsi)/(2*pi) *^ so3 0 2
    stressTensorRep = B3d.ConformalRep (RelativeUnitarity 0) 2
    dPsi = fromRational (reflect @s Psi)

bulkConstraints :: FourFermions3d -> [BulkConstraint]
bulkConstraints f = do
  parity <- [minBound .. maxBound]
  l <- case parity of
    B3d.ParityEven -> filter even (spins f)
    B3d.ParityOdd  -> spins f
  (delta, range) <- listDeltas (l, parity) (spectrum f)
  let internalRep = B3d.ConformalRep delta (fromIntegral l)
  pure $ case (parity, even l, l) of
    (B3d.ParityEven, True, 0)  -> BulkConstraint range $ ParityEven_Scalar delta
    (B3d.ParityEven, True, _)  -> BulkConstraint range $ ParityEven_SpinEven internalRep
    (B3d.ParityOdd,  True, _)  -> BulkConstraint range $ ParityOdd_SpinEven internalRep
    (B3d.ParityOdd,  False, _) -> BulkConstraint range $ ParityOdd_SpinOdd internalRep
    _ -> error "Internal representation disallowed"

ffSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext B3d.Block3d a m
     )
  => FourFermions3d
  -> SDP.SDP m a
ffSDP f@FourFermions3d{..} = runTagged deltaPsi $ do
  let dv = derivsVecFF f
  bulk <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  unit <- mat IdentityChannel
  stress <- mat StressTensorChannel
  let
    stressConstraint = BSDP.isolatedConstraint blockParams dv stress
    (cons, obj, norm) = case objective of
      Feasibility ->
        ( stressConstraint : bulk
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
      StressTensorOPEBound dir ->
        ( bulk
        , BSDP.bootstrapObjective blockParams dv unit
        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
        )
  return $ SDP.SDP
    { SDP.sdpObjective     = obj
    , SDP.sdpNormalization = norm
    , SDP.sdpMatrices      = cons
    }

instance ToSDP FourFermions3d where
  type SDPFetchKeys FourFermions3d = '[ B3d.BlockTableKey ]
  toSDP = ffSDP

instance SDPFetchBuildConfig FourFermions3d where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . B3d.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig cftBoundFiles

instance Static (Binary FourFermions3d)              where closureDict = cPtr (static Dict)
instance Static (Show FourFermions3d)                where closureDict = cPtr (static Dict)
instance Static (ToSDP FourFermions3d)               where closureDict = cPtr (static Dict)
instance Static (ToJSON FourFermions3d)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig FourFermions3d) where closureDict = cPtr (static Dict)
instance Static (BuildInJob FourFermions3d)          where closureDict = cPtr (static Dict)
