{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Bounds.SingletScalarBlocks3d where

import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Coerce                            (coerce)
import           Data.Data                              (Typeable)
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged)
import           Data.Traversable                       (for)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (BlockFetchContext,
                                                         Coordinate (WS),
                                                         CoordinateDir (XDir, YDir),
                                                         CrossingMat,
                                                         Delta (..),
                                                         Derivative (..),
                                                         Sign (..),
                                                         TaylorCoeff (..),
                                                         Taylors, xOdd,
                                                         xtTaylors, yEven)
import qualified SDPB.Blocks.Blocks3d                   as B3d
import           SDPB.Blocks.Blocks3d.Build             (block3dBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
--import           SDPB.Bounds.BoundDirection             (BoundDirection,
--                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         crossingMatrix,
                                                         derivsVec, mapBlocks,
                                                         opeCoeffIdentical_,
                                                         runTagged)
import           SDPB.Bounds.Spectrum                   (DeltaRange, Spectrum,
                                                         listDeltas)
import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Build.Fetches                     (FetchConfig (..))
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import           SDPB.Math.HalfInteger                  (HalfInteger)
import           SDPB.Math.Linear.Literal               (toV)
import           SDPB.Math.VectorSpace                  (zero, (*^))
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = Phi
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s Rational) => HasRep (ExternalOp s) (B3d.ConformalRep Rational) where
  rep phi = B3d.ConformalRep (reflect phi) 0

data SingletScalar = SingletScalar
  { deltaPhi    :: Rational
  , spectrum    :: Spectrum Int
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
--  | StressTensorOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- | A channel with crossing matrices of size jxj containing blocks of
-- type 'b'
data Channel j b where
  SpinEven :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  IdentityChannel     :: Channel 1 B3d.IdentityBlock3d
  StressTensorChannel :: Channel 1 B3d.Block3d

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: KnownNat j -- ^ Existentially quantify over j
    => DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel j B3d.Block3d -- ^ The associated Channel
    -> BulkConstraint
--
---- | Equations A.10 in https://arxiv.org/pdf/1612.08987.pdf [1]
----
---- Note that the conventions for x,t coordinates in [1] are different
---- from what blocks_3d uses. Firstly, blocks_3d defines t = ((z -
---- zb)/2)^2, whereas [1] uses (z-zb)/2. Furthermore, as described in
---- https://gitlab.com/bootstrapcollaboration/blocks_3d/-/blob/master/doc/conventions.pdf,
---- blocks_3d supplies an extra factor of p_-(x,t)=2/(z-zb) when the
---- 4pt sign is Minus. Thus, the conversion is as follows:
----
---- > \partial_t^{2m}   from [1] -> \partial_t^m when 4pt sign is Plus
---- > \partial_t^{2m+1} from [1] -> \partial_t^m when 4pt sign is Minus
----
---- We include an optional flavor sign 'fSign' for the x-derivatives,
---- since this is needed in the GNY model.

wsTaylors :: Sign 'XDir -> Sign 'YDir -> Taylors 'WS
wsTaylors = coerce xtTaylors

crossingEqs
  :: forall s a b .
     FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 1 (Taylors 'WS, FreeVect b a)
crossingEqs g = toV
  (wsTaylors xOdd yEven,  g Phi Phi Phi Phi (B3d.Q4Struct (0,0,0,0) Plus))

derivsVecSS :: SingletScalar -> V 1 (Vector (TaylorCoeff (Derivative 'WS)))
derivsVecSS f =
  fmap ($ B3d.nmax (blockParams f)) (derivsVec crossingEqs)

crossingMatSS
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (B3d.SO3Struct Rational Rational Delta) a)
  -> Tagged s (CrossingMat j 1 B3d.Block3d a)
crossingMatSS channel =
  pure $ mapBlocks B3d.Block3d $
  crossingMatrix channel (crossingEqs @s)

so3 :: (Num a, Eq a) => HalfInteger -> HalfInteger -> FreeVect B3d.SO3StructLabel a
so3 j12 j123 = vec (B3d.SO3StructLabel j12 j123)

mat
  :: forall s a j b . (Reifies s Rational, Floating a, Eq a)
  => Channel j b
  -> Tagged s (CrossingMat j 1 b a)
mat (SpinEven internalRep) = crossingMatSS $
  opeCoeffIdentical_ Phi internalRep <$> toV (so3 0 l)
  where
    l = B3d.spin internalRep

mat IdentityChannel =
  pure $ mapBlocks B3d.IdentityBlock3d $
  crossingMatrix (toV identityOpe) (crossingEqs @s)
  where
    identityOpe o1 o2
      | o1 == o2  = vec (rep @(ExternalOp s) o1)
      | otherwise = 0

mat StressTensorChannel = crossingMatSS $
  opeCoeffIdentical_ Phi stressTensorRep <$> toV stressTensorStruct
  where
    stressTensorStruct = dPhi *^ so3 0 2
    stressTensorRep = B3d.ConformalRep (RelativeUnitarity 0) 2
    dPhi = fromRational (reflect @s Phi)

bulkConstraints :: SingletScalar -> [BulkConstraint]
bulkConstraints f = do
  l <- filter even (spins f)
  (delta, range) <- listDeltas l (spectrum f)
  let internalRep = B3d.ConformalRep delta (fromIntegral l)
  pure $ BulkConstraint range $ SpinEven internalRep

ssSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext B3d.Block3d a m
     )
  => SingletScalar
  -> SDP.SDP m a
ssSDP f@SingletScalar{..} = runTagged deltaPhi $ do
  let dv = derivsVecSS f
  bulk <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  unit <- mat IdentityChannel
  stress <- mat StressTensorChannel
  let
    stressConstraint = BSDP.isolatedConstraint blockParams dv stress
    (cons, obj, norm) = case objective of
      Feasibility ->
        ( stressConstraint : bulk
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv unit
        )
--      StressTensorOPEBound dir ->
--        ( bulk
--        , BSDP.bootstrapObjective blockParams dv unit
--        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
--        )
  return $ SDP.SDP
    { SDP.sdpObjective     = obj
    , SDP.sdpNormalization = norm
    , SDP.sdpMatrices      = cons
    }

instance ToSDP SingletScalar where
  type SDPFetchKeys SingletScalar = '[ B3d.BlockTableKey ]
  toSDP = ssSDP

instance SDPFetchBuildConfig SingletScalar where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . B3d.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig cftBoundFiles

instance Static (Binary SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (Show SingletScalar)                where closureDict = cPtr (static Dict)
instance Static (ToSDP SingletScalar)               where closureDict = cPtr (static Dict)
instance Static (ToJSON SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig SingletScalar) where closureDict = cPtr (static Dict)
instance Static (BuildInJob SingletScalar)          where closureDict = cPtr (static Dict)
