module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.IsingSigEpsTest2020
import qualified Projects.FourFermions3dTest2020
import qualified Projects.GNYTest2020
import qualified Projects.GNYSigPsiEpsTest2020
import qualified Projects.GNYPsiEpsTest2020
import qualified Projects.SingletScalar2020
import qualified Projects.SingletScalarBlocks3d2020
import qualified Projects.JJJJ4d2020

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [ Projects.IsingSigEpsTest2020.boundsProgram
  , Projects.FourFermions3dTest2020.boundsProgram
  , Projects.GNYTest2020.boundsProgram
  , Projects.GNYPsiEpsTest2020.boundsProgram
  , Projects.GNYSigPsiEpsTest2020.boundsProgram
  , Projects.SingletScalar2020.boundsProgram
  , Projects.SingletScalarBlocks3d2020.boundsProgram
  , Projects.JJJJ4d2020.boundsProgram
  ]
